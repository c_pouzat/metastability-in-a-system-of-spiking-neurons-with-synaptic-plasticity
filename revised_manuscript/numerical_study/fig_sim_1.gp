set terminal pngcairo size 1000,640 font "sans,16"
set output "figs/fig_sim_1.png"
unset key
set grid
set title "Number of active neurons among 50 (θ=5)"
set ylabel "Number of neurons"
set xlabel "Time"
plot [0:450] [0:50] 'simulations/sim_1_for_fig_1' using 1:($12+$13) with steps lc 'black'
