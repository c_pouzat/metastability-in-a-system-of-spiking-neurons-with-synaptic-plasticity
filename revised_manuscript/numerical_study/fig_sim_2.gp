set terminal pngcairo size 1000,640 font "sans,16"
set output "figs/fig_sim_2.png"
set multiplot title "3 epochs" layout 1,3 margins 0.1,0.9,0.1,0.9 spacing 0.05
unset key
set grid
set ylabel "Number of neurons"
set xlabel "Time"
plot [0:5] [0:50] 'simulations/sim_1_for_fig_1' \
     using 1:12 with steps lc 'blue',\
     '' using 1:13 with steps lc 'orange',\
     '' using 1:($12+$13) with steps lc 'black'
set ytics format ""
set ylabel ""
set xlabel "Time"
plot [200:205] [0:50] 'simulations/sim_1_for_fig_1' \
     using 1:12 with steps lc 'blue',\
     '' using 1:13 with steps lc 'orange',\
     '' using 1:($12+$13) with steps lc 'black'
set ytics format ""
set ylabel ""
set xlabel "Time"
plot [441:446] [0:50] 'simulations/sim_1_for_fig_1' \
     using 1:12 with steps lc 'blue',\
     '' using 1:13 with steps lc 'orange',\
     '' using 1:($12+$13) with steps lc 'black'
unset multiplot
