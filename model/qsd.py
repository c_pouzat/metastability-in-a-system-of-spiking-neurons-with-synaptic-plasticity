#!/bin/env python3
import argparse
from math import fsum

# Read input parameters
script_description = ("Computes the QSD of an homogenous network made of\n"
                      "N neurons.\n"
                      "Two variables are associated with each neuron:\n"
                      "  a \"membrane potential\" U,\n"
                      "  a \"synaptic facilitation\" F.\n"
                      "The neurons spike independently of each other with a\n"
                      "rate beta once their membrane potential equals or\n"
                      "exceeds a threshold theta.\n"
                      "The neuron that spiked increases the membrane potential\n"
                      "of all the other neurons by 1 if it's synapse was\n"
                      "facilitated, O otherwise.\n"
                      "After the spike the facilitation of the neuron\n"
                      "that spiked is set to 1 (facilitated) if it was 0\n"
                      "(unfacilitated) and stays at 1 otherwise.\n"
                      "The membrane potential of the neuron that just spiked\n"
                      "is set to 0.\n"
                      "Independently of spike events the facilitation of each\n"
                      "neuron drops to 0 exponentially with rate lambda.\n")
parser = argparse.ArgumentParser(description=script_description,
                                 formatter_class=argparse.RawDescriptionHelpFormatter)
# Start with the number of neurons
def _nb_neurons(string):
    n = int(string)
    if n <= 0:
        msg = "The number of neurons must be > 0"
        raise argparse.ArgumentTypeError(msg)
    return n
parser.add_argument('-n', '--network-size',
                    type=_nb_neurons, dest='Nb',
                    help=('The network size (default 100)'),
                    default=50)
# Set l
parser.add_argument('-l', '--lambda',
                    type=float, dest='lmbda',
                    help=('synaptic facilitation decay rate (default 50)'),
                    default=6.7)

# Set beta
parser.add_argument('-b', '--beta',
                    type=float, dest='beta',
                    help=('spike rate for neurons above threshold (default 100)'),
                    default=10)
# Set theta
parser.add_argument('-t', '--theta',
                    type=int, dest='theta',
                    help=('membrane potential threshold for spiking'),
                    default=5)
  
args = parser.parse_args()
Nb = args.Nb
beta = args.beta
lmbda = args.lmbda
theta = args.theta


# Define target function
def target(x):
    res = pow((beta*x)/(lmbda+beta*x),theta)
    return Nb*beta/(lmbda+beta)*res-theta-x

def d_target(x):
    res = pow((beta*x)/(lmbda+beta*x),theta-1)
    res *= Nb*beta/(lmbda+beta)*theta
    res *= beta*lmbda/(lmbda+beta*x)**2
    res -= 1.0
    return res

# Define bisection
def bisection():
    nmax = 1000
    tol = 1e-10
    right = Nb
    r_val = target(right)
    left = Nb*0.2
    l_val = target(left)
    n = 0
    while n < nmax:
        mid = 0.5*(right+left)
        m_val = target(mid) 
        if abs(m_val) < 1e-10 or (right-left)*0.5 < tol:
            # print("Nb iter: {0}, target: {1}, est: {2}".format(n,m_val,mid))
            return mid
        n += 1
        if m_val * r_val > 0:
            right = mid
            r_val = m_val
        else:
            left = mid
            l_val = m_val


# Define Newton method
def newton():
    tol = 1e-10
    x = Nb
    fx = target(x)
    while abs(fx) > tol:
        x -= fx/d_target(x)
        fx = target(x)
    # print("Target: {0}, derivative: {1}, estimate: {2}".format(fx,d_target(x),x))    
    return x

# Compute QSD
#mu_theta1 = bisection()
mu_theta1 = newton()
rho_0 = beta*mu_theta1/(lmbda+beta*mu_theta1)
kappa = Nb/(theta+mu_theta1)
mu_thetaDot = mu_theta1 * kappa
mu_theta0 = mu_thetaDot - mu_theta1
qsd = [0 for i in range(2*(theta+1))]
rho = rho_0
for i in range(theta):
   qsd[2*i] = kappa*(1-rho)
   qsd[2*i+1] = kappa*rho
   print(i,qsd[2*i],qsd[2*i+1])
   rho *= rho_0
qsd[2*theta] = mu_theta0
qsd[2*theta+1] = mu_theta1
print(theta,qsd[2*theta],qsd[2*theta+1])
# print(fsum(qsd))
