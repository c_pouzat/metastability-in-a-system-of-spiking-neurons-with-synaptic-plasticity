# A Quasi-Stationary Approach to Metastability in a System of Spiking Neurons with Synaptic Plasticity

This project contains material associated with the manuscript _A Quasi-Stationary Approach to Metastability in a System of Spiking Neurons with Synaptic Plasticity_ by Morgan André and Christophe Pouzat. This is a cleaned-up project with the (nearly) "final" versions of all the files it contains. If you want to see the whole project history, check our `PlmLab` project: [markov-chains-with-variable-length-memories-exploration](https://plmlab.math.cnrs.fr/xtof/markov-chains-with-variable-length-memories-exploration).

## Content

### Top level
The first version of the manuscript entitled _Metastability in a System of Spiking Neurons with Synaptic Plasticity_ ([hal-03281732 , version 2](https://hal.science/hal-03281732v2)) is in file `Andre_Pouzat_MSSNSP.pdf`, the `TeX` source file in `Andre_Pouzat_MSSNSP.tex` and the `bib` file in `Andre_Pouzat_MSSNSP.bib`, all located at top level. All figures can be found in folder `simulations/figs`.

The **Makefile** contains _all the instructions necessary for the regeneration of all the figures of the new manuscript version_. This means that the simulations and processing required for the figures are also done by this `Makefile`. It downloads all the custom developed codes in `C`, `Fortran`, `Python`, `gnuplot` and `dot` from this (and other `GitHub`) repository. To run it you will need [`gcc`](https://gcc.gnu.org/) (I used version 14.1.0), [`gfortran`](https://gcc.gnu.org/wiki/GFortran) (I used version 14.1.0), [`Python 3`](https://www.python.org/) (I used version 3.9.18) and [`matplotlib`](https://matplotlib.org/), [`gnuplot`](http://www.gnuplot.info/) (I used version 5.4) and [`graphviz / dot`](https://graphviz.org/) (I used version 2.43.0). If you have these programs installed, you just need to download the `Makefile` and type `make` in the directory where you downloaded it; then wait for roughly 1 hour and 15 minutes (running on a laptop with a 12th generation Intel core i7-1255U processor) and you will have sub-directories `code`, `simulations`, `explicit` and `figs` created and populated. The figures will be in `figs`.  Typing `make check` will then make sure that you simulated _exactly_ (bit for bit compatibility) the same data as we did.

### `revised_manuscript`

Contains the source files of the revised version of the manuscript ([hal-03281732 , version 3 ](https://hal.science/hal-03281732v3)).

### `code`
Folder `code` contains the `Python`, `C` and `Fortran` codes used for the simulations:

- `stoch_simple_stf_small_scale.c`
- `stoch_simple_stf.c`
- `stoch_simple_stf_rep.c`
- `ntw_stp.py`
- `ntw_stp.f90`
- `ntw_ecdf.f90`

The `C` codes are fully described in file `simulation_code_definitions.pdf`, while the `Python` and `Fortran` codes are described in file `new_simulation_code_def.pdf`. Files `simulation_code_definitions.org` and `new_simulation_code_def.org` are the `emacs` `orgmode` source file of both the description and the codes. The folder contains also:

- `mu_E-gsl-with-arg.c`

source of the program returning the network properties in the metastable state. File `implicit_equation_code.pdf` contains a description of the code and file `implicit_equation_code.org` is the `emacs` `orgmode` source file of both the description and the codes.

The `Fortran` code makes use of Jannis Teunissen `m_random` module from `GitHub` [rng_fortran](https://github.com/jannisteunissen/rng_fortran) repository.

### `simulations`

Folder `simulations` contains the scripts running the simulations (as `makefiles` in dedicated subfolders) as well as `Python` scripts analysing them and generating the figures. File `simulations_and_figs.pdf` contains a detailed description of all that and file `simulations_and_figs.org` is the `emacs` `orgmode` source file of both the description and the codes.

## Required software

Our simulation and numerical integration codes are written in `C` or `Fortran`. The simulation codes do not need anything beyond the `standard C library` and a modern compiler like the one we used here, [`gcc`](https://gcc.gnu.org/), is enough to generate the binaries (we used version `11.1.0`). The numerical integration code (`mu_E-gsl-with-arg.c`) depends on the `Gnu Scientific Library` ([GSL](https://www.gnu.org/software/gsl/), version `2.6` was used).

The analysis and plotting codes and scripts require [`Python 3`](https://www.python.org/) (version `3.9.5` was used) as well as [`numpy`](https://numpy.org/) (version `1.20.3` was used), [`scipy`](https://www.scipy.org/) (version `1.6.3` was used) and [`matplotlib`](https://matplotlib.org/) (version `3.4.1` was used).

The `Fortan` code requires a modern compiler like `gfortran`, the figures of the `Fortran` code documentation were produced with `gnuplot`. The use of normalized compiled languages like `C` or `Fortran`, as well as the use of `gnuplot` (as opposed to `Python`), are motivated by a need for "reprodicibility in time". _Do not use `Python` for anything else than preliminary work_.
