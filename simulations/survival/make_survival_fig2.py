def read_survival(prefix):
    """Reads content of file prefix_survival and stores result in a list.

    Parameters
    ----------
    prefix: the "prefix" of the file's name (a string). The actual file
            name should be prefix_survival.

    Returns
    -------
    A list with the survival times
    """
    file_name = prefix+"_survival"
    survival_time = []
    with open(file_name,'r') as f:
        line = f.readline() # read first line
        N = int(line.split()[6])
        for i in range(4):
            line = f.readline() # read next 4 lines
        theta = float(line.split()[4])
        line = f.readline() # read next line
        beta = float(line.split()[4])
        line = f.readline() # read next line
        lambda_ = float(line.split()[4])
        line = f.readline() # read next line
        duration = float(line.split()[4])
        msg = "*** Networks of {0:d} neurons.\n".format(N)
        msg += "*** Threshold of: {0:f},\n".format(theta)
        msg += "*** beta = {0:f},\n".format(beta)
        msg += "*** lambda = {0:f}.\n".format(lambda_)
        msg += "*** Maximal duration: {0:f}.\n".format(duration)
        print(msg)
        for line in f:
            if not (line[0] in {"#","\n"}):
                cuts = line.split()
                survival_time.append(float(cuts[0]))
    return survival_time

import matplotlib.pylab as plt
plt.style.use('ggplot')
yy = [1-i/1000 for i in range(1,1001)]
s7p0 = read_survival("sim_n50_f0p75_u50_l7_b10")
min_val = min(s7p0)
s7p0 = sorted([x-min_val for x in s7p0])
plt.step(s7p0,yy,where='post',color='black')
for i in range(8,19):
    f_name = "sim_n50_f0p75_u50_l"+str(i)+"_b10"
    surv = read_survival(f_name)
    min_val = min(surv)
    surv = sorted([x-min_val for x in surv])
    plt.step(surv,yy,where='post',color='grey')
plt.xlim([0,5])
plt.ylim([0.05,1])
plt.yscale('log')
plt.xlabel('Survival time')
plt.ylabel('Fraction still alive')
plt.savefig('../figs/survival-fig2.png')
plt.close()
