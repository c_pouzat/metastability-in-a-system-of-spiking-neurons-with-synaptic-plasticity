def read_survival(prefix):
    """Reads content of file prefix_survival and stores result in a list.

    Parameters
    ----------
    prefix: the "prefix" of the file's name (a string). The actual file
            name should be prefix_survival.

    Returns
    -------
    A list with the survival times
    """
    file_name = prefix+"_survival"
    survival_time = []
    with open(file_name,'r') as f:
        line = f.readline() # read first line
        N = int(line.split()[6])
        for i in range(4):
            line = f.readline() # read next 4 lines
        theta = float(line.split()[4])
        line = f.readline() # read next line
        beta = float(line.split()[4])
        line = f.readline() # read next line
        lambda_ = float(line.split()[4])
        line = f.readline() # read next line
        duration = float(line.split()[4])
        msg = "*** Networks of {0:d} neurons.\n".format(N)
        msg += "*** Threshold of: {0:f},\n".format(theta)
        msg += "*** beta = {0:f},\n".format(beta)
        msg += "*** lambda = {0:f}.\n".format(lambda_)
        msg += "*** Maximal duration: {0:f}.\n".format(duration)
        print(msg)
        for line in f:
            if not (line[0] in {"#","\n"}):
                cuts = line.split()
                survival_time.append(float(cuts[0]))
    return survival_time

def surv_exp_fit(obs,
                 censoring_time=500,
                 ci=0.95):
    """Fit an exponential model to the potentially right censored data in 'obs'

    The minimal observed duration is substracted first from all elements in obs

    Parameters
    ----------
    obs: a list with the survival times
    censoring_time: the censoring time
    ci: the level of the confidence interval

    Returns
    -------
    A tuple with:
      the lower bound of the CI
      the estimated value
      the upper bound of the CI
    """
    from math import log
    from scipy.stats import chi2
    from scipy.optimize import brentq
    min_obs = min(obs)
    n_full = sum([x < censoring_time for x in obs]) # nb of full observations
    data = [x-min_obs for x in obs]
    censoring_time -= min_obs
    data_sum = sum(data)
    nu_hat = n_full / data_sum
    def log_lik(nu):
        return n_full*log(nu) - nu*data_sum
    threshold = chi2.ppf(ci,1)/2
    log_lik_at_nu_hat = log_lik(nu_hat)
    def target(nu):
        return log_lik_at_nu_hat - log_lik(nu) - threshold
    CI_lwr = brentq(target,0.2*nu_hat,nu_hat)
    CI_upr = brentq(target,nu_hat,3*nu_hat)
    return (CI_lwr,nu_hat,CI_upr)

n_list = [i*10 for i in range(3,13)]
nu_lst = []
for n in n_list:
    f_name = "sim_n"+str(n)+"_f0p75_l7_b10"
    surv = read_survival(f_name)
    nu_tpl = surv_exp_fit(surv,30000)
    nu_lst.append(nu_tpl)
lwr_lst = [1/nu[2] for nu in nu_lst]
upr_lst = [1/nu[0] for nu in nu_lst]
mu = [1/nu[1] for nu in nu_lst]
import matplotlib.pylab as plt
import numpy as np
plt.style.use('ggplot')
plt.errorbar(n_list,mu,
             yerr=np.array([lwr_lst,upr_lst]),
             fmt='none')
plt.yscale('log')
plt.xlabel(r'Network size')
plt.ylabel('Mean time to extinction')
plt.savefig('../figs/survival-fig4.png')
plt.close()
