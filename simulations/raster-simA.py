def read_ntw(prefix):
    """Reads content of file prefix_ntw and stores result in a tuple of
    binary arrays.

    Parameters
    ----------
    prefix: the "prefix" of the file's name (a string). The actual file
            name should be prefix_ntw.

    Returns
    -------
    A tuple with five binary arrays:
    - the spike times (double)
    - the neuron of origin (int)
    - the number of neurons above threshold after the spike
    - the number of facilitated synapses after the spike
    - a Boolean: 0 if the synapse was not facilitated at the time of
                 the spike and 1 otherwise
    """
    import array
    file_name = prefix+"_ntw"
    spike_time = [] 
    spike_origin = []
    n_theta = []
    f_sum = []
    facilitated = []
    for line in open(file_name,'r'):
        if not (line[0] in {"#","\n"}):
            cuts = line.split()
            spike_time.append(float(cuts[0]))
            spike_origin.append(int(cuts[2]))
            n_theta.append(int(cuts[3]))
            f_sum.append(int(cuts[4]))
            facilitated.append(int(cuts[5]))
    nb_neuron = max(spike_origin)+1
    mes = ("{0} spikes read from {1} neurons.\n"
           "The first spike occurred at: {2},\n"
           "The last spike occurred at:  {3}")
    print(mes.format(len(spike_time),
                     nb_neuron,
                     spike_time[0],
                     spike_time[-1]))
    return array.array('d',spike_time),\
        array.array('I',spike_origin),\
        array.array('I',n_theta),\
        array.array('I',f_sum),\
        array.array('I',facilitated)

def raster(sipke_time,
           spike_origin,
           syn_status,
           color_on = "blue",
           color_off = "orange",
           markersize=0.3):
    plt.style.use('default')
    N = max(spike_origin)+1
    labelled_train = [[[sipke_time[i] for i in range(len(sipke_time))
                        if spike_origin[i]==n_idx and syn_status[i]==1],
                       [sipke_time[i] for i in range(len(sipke_time))
                        if spike_origin[i]==n_idx and syn_status[i]==0]]
                      for n_idx in range(N)]
    for n_idx in range(N):
        tt = labelled_train[n_idx][0]
        yy = [n_idx for i in range(len(tt))]
        plt.scatter(tt, yy, marker=".",color=color_on,s=markersize)
        tt = labelled_train[n_idx][1]
        yy = [n_idx for i in range(len(tt))]
        plt.scatter(tt, yy, marker=".",color=color_off,s=markersize)
    plt.tick_params(axis='x',          
                    which='both',      
                    bottom=False,      
                    top=False,         
                    labelbottom=False)
    plt.tick_params(axis='y',          
                    which='both',      
                    left=False,      
                    right=False,         
                    labelleft=False)
    plt.box(False)
    plt.subplots_adjust(left=0,right=1,bottom=0,top=1)

import matplotlib.pylab as plt
plt.subplot(121)
stA, soA, nA, fA, sA = read_ntw("simA")
raster(stA,soA,sA,markersize=0.1)
plt.plot([12,14],[-1,-1],color='black')
plt.xlim([0,20])
plt.ylim([-2,50])
plt.subplot(122)
raster(stA,soA,sA,markersize=4)
plt.xlim([12,14])
plt.ylim([-2,50])
plt.savefig('figs/raster-simA.png')
plt.close()
'figs/raster-simA.png'
