MAKEFLAGS += --no-builtin-rules --no-builtin-variables
CC := gcc
CFLAGS := -Wall -g -O3
RM := rm -f

all: figs/single-neuron-chain.png figs/simB-MPP-plot-large.png \
figs/simB-MPP-plot-narrow.png figs/raster-simA.png figs/raster-simB.png \
figs/simA-simB-early-CP-plots.png figs/full_states_ntw_N5.png \
figs/sec5_n5t1b10l4_fig_2.png figs/emp-and-theo-qsd-from-sec5_n5t1b10l4_gp.png \
figs/sec5_n500t100b10l5_fig_1.png figs/sec5_fig_2.png \
figs/sec5_big_fig.png

#### Reproduce Figure 1 ########################################
# Start by downloading the dot file; this requires wget
figs/single-neuron-chain.dot:
	mkdir figs
	cd figs && wget https://gitlab.com/c_pouzat/metastability-in-a-system-of-spiking-neurons-with-synaptic-plasticity/-/raw/main/code/figs/single-neuron-chain.dot

# Make figure from .dot source file
# This requires Graphviz
figs/single-neuron-chain.png: figs/single-neuron-chain.dot
	cd figs && dot -Tpng single-neuron-chain.dot > single-neuron-chain.png
#### Figure 1 Done ########################################

#### Reproduce Figure 2 ########################################
# Start by downloading the C source file
code/stoch_simple_stf_small_scale.c:
	mkdir code
	cd code && wget https://gitlab.com/c_pouzat/metastability-in-a-system-of-spiking-neurons-with-synaptic-plasticity/-/raw/main/code/stoch_simple_stf_small_scale.c

# Compile the code
code/stoch_simple_stf_small_scale: code/stoch_simple_stf_small_scale.c
	cd code && gcc -Wall -g -O0 -o stoch_simple_stf_small_scale \
stoch_simple_stf_small_scale.c -lm -std=gnu11

# Run simulations
simulations/simB_ntw simulations/simB_neuron:  code/stoch_simple_stf_small_scale
	mkdir simulations
	code/stoch_simple_stf_small_scale -s 18710305 -g 1857005 -d 50 --out-name=simulations/simB -n 50 -u 50 -f 0.75 -l 6.7 -b 10

simulations/simA_ntw simulations/simA_neuron:  code/stoch_simple_stf_small_scale
	code/stoch_simple_stf_small_scale -s 18710305 -g 1857075 -d 50 --out-name=simulations/simA -n 50 -u 50 -f 0.75 -l 6.7 -b 10

# Get Python script simB-MPP-plot-large.py
code/simB-MPP-plot-large.py:
	cd code && wget https://gitlab.com/c_pouzat/metastability-in-a-system-of-spiking-neurons-with-synaptic-plasticity/-/raw/main/code/simB-MPP-plot-large.py

# Make the figure
figs/simB-MPP-plot-large.png : simulations/simB_ntw simulations/simB_neuron \
code/simB-MPP-plot-large.py
	python3 code/simB-MPP-plot-large.py
#### Figure 2 Done ########################################

#### Reproduce Figure 3 ########################################
# Get Python script simB-MPP-plot-narrow.py
code/simB-MPP-plot-narrow.py:
	cd code && wget https://gitlab.com/c_pouzat/metastability-in-a-system-of-spiking-neurons-with-synaptic-plasticity/-/raw/main/code/simB-MPP-plot-narrow.py

# Make the figure
figs/simB-MPP-plot-narrow.png : simulations/simB_ntw simulations/simB_neuron \
code/simB-MPP-plot-narrow.py
	python3 code/simB-MPP-plot-narrow.py
#### Figure 3 Done ########################################

#### Reproduce Figure 4 ########################################
# Get Python script raster-simA.py
code/raster-simA.py:
	cd code && wget https://gitlab.com/c_pouzat/metastability-in-a-system-of-spiking-neurons-with-synaptic-plasticity/-/raw/main/code/raster-simA.py

# Make the figure
figs/raster-simA.png: simulations/simA_ntw simulations/simA_neuron \
code/raster-simA.py
	python3 code/raster-simA.py
#### Figure 4 done      ########################################

#### Reproduce Figure 5 ########################################
# Get Python script raster-simB.py
code/raster-simB.py:
	cd code && wget https://gitlab.com/c_pouzat/metastability-in-a-system-of-spiking-neurons-with-synaptic-plasticity/-/raw/main/code/raster-simB.py

# Make the figure
figs/raster-simB.png: simulations/simB_ntw simulations/simB_neuron \
code/raster-simB.py
	python3 code/raster-simB.py
#### Figure 5 done      ########################################

#### Reproduce Figure 6 ########################################
# Get Python script simA-simB-early-CP-plots.py
code/simA-simB-early-CP-plots.py:
	cd code && wget https://gitlab.com/c_pouzat/metastability-in-a-system-of-spiking-neurons-with-synaptic-plasticity/-/raw/main/code/simA-simB-early-CP-plots.py

# Make the figure
figs/simA-simB-early-CP-plots.png: simulations/simB_ntw simulations/simB_neuron \
simulations/simA_ntw simulations/simA_neuron code/simA-simB-early-CP-plots.py
	python3 code/simA-simB-early-CP-plots.py
#### Figure 6 done      ########################################

#### Reproduce Figure 8 ########################################
# Get conf_generator.f90 source file
code/conf_generator.f90:
	cd code && wget https://gitlab.com/c_pouzat/metastability-in-a-system-of-spiking-neurons-with-synaptic-plasticity/-/raw/main/code/conf_generator.f90

# Compile it
code/conf_generator: code/conf_generator.f90
	cd code && gfortran -o conf_generator conf_generator.f90

# Create dot file for a network with 5 neurons
figs/full_states_ntw_N5.dot: code/conf_generator
	code/conf_generator 5 > figs/full_states_ntw_N5.dot

# Convert .dot into .png
figs/full_states_ntw_N5.png: figs/full_states_ntw_N5.dot
	cd figs && dot -Tpng full_states_ntw_N5.dot > full_states_ntw_N5.png
#### Figure 8 done      ########################################

#### Reproduce Figure 9 ########################################
# Get m_random.f90 source
code/m_random.f90:
	cd code && wget https://gitlab.com/c_pouzat/metastability-in-a-system-of-spiking-neurons-with-synaptic-plasticity/-/raw/main/code/m_random.f90

# We compile it with
code/m_random.o: code/m_random.f90
	gfortran -c -o code/m_random.o code/m_random.f90 -O3 -flto -g -std=f2008 -Wall -Wextra -fopenmp

# Get ntw_stp.f90 source file
code/ntw_stp.f90:
	cd code && wget https://gitlab.com/c_pouzat/metastability-in-a-system-of-spiking-neurons-with-synaptic-plasticity/-/raw/main/code/ntw_stp.f90

# Compile it
code/ntw_stp: code/m_random.o code/ntw_stp.f90
	gfortran -c -o code/ntw_stp.o code/ntw_stp.f90 -O3 -flto -g -std=f2008 -Wall -Wextra -fopenmp
	gfortran -o code/ntw_stp code/ntw_stp.o code/m_random.o -O3 -flto -g -std=f2008 -Wall -Wextra -fopenmp

# Do the simulation
simulations/sec5_n5t1b10l4: code/ntw_stp
	code/ntw_stp 5 10 4 1 5 0.01 100000 20061001 > simulations/sec5_n5t1b10l4

# Get gnuplot script file sec5_n5t1b10l4_fig_2.gp
figs/sec5_n5t1b10l4_fig_2.gp:
	cd figs && wget https://gitlab.com/c_pouzat/metastability-in-a-system-of-spiking-neurons-with-synaptic-plasticity/-/raw/main/code/figs/sec5_n5t1b10l4_fig_2.gp

# Make the figure
figs/sec5_n5t1b10l4_fig_2.png: figs/sec5_n5t1b10l4_fig_2.gp simulations/sec5_n5t1b10l4
	gnuplot figs/sec5_n5t1b10l4_fig_2.gp
#### Figure 9 done      ########################################

#### Reproduce Figure 10 #######################################
# Get explicit/Makefile
explicit/Makefile:
	mkdir explicit
	cd explicit && wget https://gitlab.com/c_pouzat/metastability-in-a-system-of-spiking-neurons-with-synaptic-plasticity/-/raw/main/code/explicit/Makefile

# Get lossy_generator.f90 source file
explicit/lossy_generator.f90:
	cd explicit && wget https://gitlab.com/c_pouzat/metastability-in-a-system-of-spiking-neurons-with-synaptic-plasticity/-/raw/main/code/explicit/lossy_generator.f90

# Get quasi_stationary_dist.f90 source code
explicit/quasi_stationary_dist.f90:
	cd explicit && wget https://gitlab.com/c_pouzat/metastability-in-a-system-of-spiking-neurons-with-synaptic-plasticity/-/raw/main/code/explicit/quasi_stationary_dist.f90

explicit/lossy_generator: explicit/Makefile explicit/lossy_generator.f90
	cd explicit && $(MAKE) MAKEFLAGS= lossy_generator

explicit/lossy_generator_N5b10l4.d:  explicit/lossy_generator
	cd explicit && ./lossy_generator 5 10 4 > lossy_generator_N5b10l4.d

explicit/librefblas.a: explicit/Makefile
	cd explicit && $(MAKE) -j MAKEFLAGS= librefblas.a

explicit/liblapack.a: explicit/Makefile
	cd explicit && $(MAKE) -j MAKEFLAGS= liblapack.a

explicit/libaux.a: explicit/Makefile
	cd explicit && $(MAKE) -j MAKEFLAGS= libaux.a

explicit/quasi_stationary_dist: explicit/Makefile explicit/quasi_stationary_dist.f90 
	cd explicit && $(MAKE) MAKEFLAGS= quasi_stationary_dist

explicit/quasi_stationary_N5b10l4.r: explicit/quasi_stationary_dist \
explicit/lossy_generator_N5b10l4.d
	cd explicit && $(MAKE) MAKEFLAGS= quasi_stationary_N5b10l4.r

# Get Python module qsd.py
figs/qsd.py:
	cd figs && wget https://gitlab.com/c_pouzat/metastability-in-a-system-of-spiking-neurons-with-synaptic-plasticity/-/raw/main/code/qsd.py

# Get Python processing code empirical-qsd-from-sec5_n5t1b10l4.py
figs/empirical-qsd-from-sec5_n5t1b10l4.py:
	cd figs && wget https://gitlab.com/c_pouzat/metastability-in-a-system-of-spiking-neurons-with-synaptic-plasticity/-/raw/main/code/figs/empirical-qsd-from-sec5_n5t1b10l4.py

# Run the code
simulations/sec5_n5t1b10l4_stats: figs/empirical-qsd-from-sec5_n5t1b10l4.py \
simulations/sec5_n5t1b10l4 explicit/quasi_stationary_N5b10l4.r figs/qsd.py
	python3 figs/empirical-qsd-from-sec5_n5t1b10l4.py

# Get gnuplot script file  emp-and-theo-qsd-from-sec5_n5t1b10l4.gp
figs/emp-and-theo-qsd-from-sec5_n5t1b10l4.gp:
	cd figs && wget https://gitlab.com/c_pouzat/metastability-in-a-system-of-spiking-neurons-with-synaptic-plasticity/-/raw/main/code/figs/emp-and-theo-qsd-from-sec5_n5t1b10l4.gp

# Make the figure
figs/emp-and-theo-qsd-from-sec5_n5t1b10l4_gp.png: figs/emp-and-theo-qsd-from-sec5_n5t1b10l4.gp \
simulations/sec5_n5t1b10l4_stats 
	gnuplot figs/emp-and-theo-qsd-from-sec5_n5t1b10l4.gp
#### Figure 10 done      #######################################


#### Reproduce Figure 11 #######################################
# Start with the simulations
simulations/sec5_n5t1b10l5: code/ntw_stp
	code/ntw_stp 5 10 5 1 3 0.01 100000 20061001 > simulations/sec5_n5t1b10l5

simulations/sec5_n50t10b10l5: code/ntw_stp
	code/ntw_stp 50 10 5 10 10 0.1 100000 20061001 > simulations/sec5_n50t10b10l5

simulations/sec5_n500t100b10l5: code/ntw_stp
	code/ntw_stp 500 10 5 100 100 1 100000 20061001 > simulations/sec5_n500t100b10l5

# Get gnuplot script file sec5_n500t100b10l5_fig_1.gp
figs/sec5_n500t100b10l5_fig_1.gp:
	cd figs && wget https://gitlab.com/c_pouzat/metastability-in-a-system-of-spiking-neurons-with-synaptic-plasticity/-/raw/main/code/figs/sec5_n500t100b10l5_fig_1.gp

# Make the figure
figs/sec5_n500t100b10l5_fig_1.png: figs/sec5_n500t100b10l5_fig_1.gp \
simulations/sec5_n500t100b10l5 simulations/sec5_n50t10b10l5 \
simulations/sec5_n5t1b10l5
	gnuplot figs/sec5_n500t100b10l5_fig_1.gp
#### Figure 11 done      #######################################

#### Reproduce Figure 12 #######################################
# Make "high resolution simulations
simulations/sec5_n50t10b10l5hr: code/ntw_stp
	code/ntw_stp 50 10 5 10 1 0.01 100000 20061001 > simulations/sec5_n50t10b10l5hr

simulations/sec5_n500t100b10l5hr: code/ntw_stp
	code/ntw_stp 500 10 5 100 1 0.01 100000 20061001 > simulations/sec5_n500t100b10l5hr

figs/sec5_fig_2.gp:
	cd figs && wget https://gitlab.com/c_pouzat/metastability-in-a-system-of-spiking-neurons-with-synaptic-plasticity/-/raw/main/code/figs/sec5_fig_2.gp

figs/sec5_fig_2.png: figs/sec5_fig_2.gp \
simulations/sec5_n50t10b10l5hr simulations/sec5_n500t100b10l5hr
	gnuplot figs/sec5_fig_2.gp
#### Figure 12 done      #######################################

#### Reproduce Figure 13 #######################################
# Make the required simulations
simulations/sec5_n5t1b10l8: code/ntw_stp
	code/ntw_stp 5 10 8 1 3 0.01 100000 20061001 > simulations/sec5_n5t1b10l8

simulations/sec5_n5t1b10l3: code/ntw_stp
	code/ntw_stp 5 10 3 1 3 0.01 100000 20061001 > simulations/sec5_n5t1b10l3

figs/sec5_big_fig.gp:
	cd figs && wget https://gitlab.com/c_pouzat/metastability-in-a-system-of-spiking-neurons-with-synaptic-plasticity/-/raw/main/code/figs/sec5_big_fig.gp

figs/sec5_big_fig.png: figs/sec5_big_fig.gp simulations/sec5_n5t1b10l3\
simulations/sec5_n5t1b10l5 simulations/sec5_n5t1b10l8
	gnuplot figs/sec5_big_fig.gp
#### Figure 13 done      #######################################

check: simulations_sha256 simulations/simA_neuron simulations/simA_ntw \
simulations/simB_neuron simulations/simB_ntw \
simulations/sec5_n500t100b10l5 simulations/sec5_n500t100b10l5hr \
simulations/sec5_n50t10b10l5 simulations/sec5_n50t10b10l5hr \
simulations/sec5_n5t1b10l3 simulations/sec5_n5t1b10l4 \
simulations/sec5_n5t1b10l4_stats simulations/sec5_n5t1b10l5 \
simulations/sec5_n5t1b10l8
	sha256sum -c simulations_sha256

simulations_sha256:
	wget https://gitlab.com/c_pouzat/metastability-in-a-system-of-spiking-neurons-with-synaptic-plasticity/-/raw/main/simulations_sha256
