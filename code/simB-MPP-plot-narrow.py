def read_neuron(prefix):
    """Reads content of file prefix_neuron and stores result in a tuple of
    lists.

    Parameters
    ----------
    prefix: the "prefix" of the file's name (a string). The actual file
            name should be prefix_neuron.
    
    Returns
    -------
    A tuple with 3 lists:
    - the event times
    - the list of mpps (one sub-list per neuron)
    - the list of facilitations (one sub-list per neuron)
    """
    import array
    file_name = prefix+"_neuron"
    with open(file_name,"r") as f:
        line = f.readline() # read first line
        N = int(line.split()[6])
        for i in range(4):
            line = f.readline() # read next four line
        theta = int(float(line.split()[4]))
        evt_time = []
        mpp_lst = [[] for i in range(N)]
        f_lst = [[] for i in range(N)]
        for line in f:
            if not (line[0] in {"#","\n"}):
                cuts = line.split()
                evt_time.append(float(cuts[0]))
                for n_idx in range(N):
                    u = int(cuts[3+2*n_idx])
                    if u >= theta: u=theta
                    mpp_lst[n_idx].append(u)
                    f_lst[n_idx].append(int(cuts[4+2*n_idx]))

    return evt_time,\
        mpp_lst,\
        f_lst

evt_timeB, mppB, fB = read_neuron("simulations/simB")
def plot_mpp_plus(evt_time,mpp_lst,f_lst,
                  begin,end,
                  color_on = "blue",
                  color_off = "orange",
                  linewidth=0.3):
    plt.style.use('default')
    idx = 0
    while evt_time[idx] < begin:
       idx += 1
    first = idx
    while evt_time[idx] < end:
        idx +=1
    last = idx
    n = len(mpp_lst)
    tt = evt_time[first:last]
    theta = max(mpp_lst[0])
    for n_idx in range(n):
        mpp = [x + theta*n_idx for x in mpp_lst[n_idx][first:last]]
        plt.step(tt,mpp,where='post',color=color_on,lw=linewidth)
        f = f_lst[n_idx][first:last]
        for i in range(len(f)-1):
            if f[i] == 0:
                xx = [tt[i],tt[i+1]]
                yy = [mpp[i],mpp[i]]
                plt.plot(xx,yy,color=color_off,lw=linewidth)
                xx = [tt[i+1],tt[i+1]]
                yy = [mpp[i],mpp[i+1]]
                plt.plot(xx,yy,color=color_off,lw=linewidth)
    plt.tick_params(axis='x',          
                    which='both',      
                    bottom=False,      
                    top=False,         
                    labelbottom=False)
    plt.tick_params(axis='y',          
                    which='both',      
                    left=False,      
                    right=False,         
                    labelleft=False)
    plt.box(False)
    plt.subplots_adjust(left=0,right=1,bottom=0,top=1)

import matplotlib.pylab as plt
plot_mpp_plus(evt_timeB,mppB,fB,1.2,1.25,linewidth=0.5)
plt.savefig('figs/simB-MPP-plot-narrow.png')
plt.close()
'figs/simB-MPP-plot-narrow.png'
