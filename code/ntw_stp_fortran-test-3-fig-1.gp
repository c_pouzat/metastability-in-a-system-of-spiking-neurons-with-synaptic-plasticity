set terminal pngcairo size 640,480
set output "figs/ntw_stp_fortran_test_3_fig_1.png"
set title 'Membrane potential at least θ'
set grid
set key at 0.5,40 top left spacing 2
set xlabel 'Time'
set ylabel 'Mean nb of neurons'
set bars small
plot [] [] 'simulations/test_fortran_3' index 10000 u 1:14:(2*$26) \
     w yerrorlines pt 0 lc 'black' lw 2 title '(u,f)=(θ,1) 10000 Rep.',\
     'simulations/test_fortran_3' index 10000 u 1:13:(2*$25) \
     w yerrorlines pt 0 lc 'red' lw 2 title '(u,f)=(θ,1) 10000 Rep.',\
