Program quasi_stationary_dist

  ! Adapted from DGEEV Example
  ! DGEEV Example Program Text
  ! Copyright (c) 2018, Numerical Algorithms Group (NAG Ltd.)
  ! For licence see
  !  https://github.com/numericalalgorithmsgroup/LAPACK_Examples/blob/master/LICENCE.md

  ! .. Use Statements ..
  Use lapack_interfaces, Only: dgeev
  Use lapack_precision, Only: dp
  ! .. Implicit None Statement ..
  Implicit None
  ! .. Parameters ..
  Real (Kind=dp), Parameter :: zero = 0.0_dp
  Integer, Parameter :: nb = 64, nin = 5, nout = 6
  ! .. Local Scalars ..
  Complex (Kind=dp) :: eig
  Real (Kind=dp) :: alpha, beta, scale, vector_sum, mean_occupancy
  Integer :: i, info, j, k, lda, ldvl, lwork, n
  ! .. Local Arrays ..
  Real (Kind=dp), Allocatable :: a(:, :), vl(:, :), wi(:), work(:), wr(:)
  integer, allocatable :: states(:,:)
  Real (Kind=dp) :: dummy(1, 1)
  ! .. Intrinsic Procedures ..
  Intrinsic :: cmplx, max, maxloc, nint, sqrt, dot_product
  ! .. Executable Statements ..
  Write (nout, *) 'Perron-Frobenius eigenvalue and eigenvector'
  ! Skip heading in data file
  Read (nin, *)
  Read (nin, *) n
  lda = n
  ldvl = n
  Allocate (a(lda,n), vl(ldvl,n), wi(n), wr(n),states(n,4))

  ! Use routine workspace query to get optimal workspace.
  lwork = -1
  Call dgeev('Vectors (left)', 'No right vectors', n, a, lda, wr, wi, &
       vl, ldvl, dummy, 1, dummy, lwork, info)

  ! Make sure that there is enough workspace for block size nb.
  lwork = max((nb+2)*n, nint(dummy(1,1)))
  Allocate (work(lwork))

  ! Read the matrix A from data file
  Read (nin, *)(a(i,1:n), i=1, n)
  Read (nin, *)
  ! Read the states description
  read (nin, *) (states(i,1:4), i=1,n)
  
  ! Compute the eigenvalues and left eigenvectors of A
  Call dgeev('Vectors (left)', 'No right vectors', n, a, lda, wr, wi, &
       vl, ldvl, dummy, 1, work, lwork, info)
      
  If (info==0) Then
     ! Print solution whose eigen value real part is the largest
     j = maxloc(wr,dim=1) 
     Write (nout, *)
     If (wi(j)==zero) Then
        Write (nout, '(a,f6.3)') 'P-F eigenvalue = ', wr(j)
     Else
        eig = cmplx(wr(j), wi(j), kind=dp)
        Write (nout, 110) 'Eigenvalue(', j, ') = ', eig
     End If
     Write (nout, *)
     Write (nout, '(a)') 'P-F Eigenvector:'
     If (wi(j)==zero) Then
        ! Scale by making the sum of the elements equal to 1
        vector_sum = sum(vl(1:n,j))
        vl(1:n,j) = vl(1:n,j)/vector_sum
        do i=1,n
           write (nout,'(a,4(i0),a,f6.4)') "Prob. of state (", states(i,1:4),"):  ", vl(i,j)
        end do   
     Else If (wi(j)>0.0E0_dp) Then
        !  Scale by making largest element real and positive
        work(1:n) = vl(1:n, j)**2 + vl(1:n, j+1)**2
        k = maxloc(work(1:n), 1)
        scale = sqrt(work(k))
        work(1:n) = vl(1:n, j)
        alpha = vl(k, j)/scale
        beta = vl(k, j+1)/scale
        vl(1:n, j) = alpha*work(1:n) + beta*vl(1:n, j+1)
        vl(1:n, j+1) = alpha*vl(1:n, j+1) - beta*work(1:n)
        Write (nout, 140)(vl(i,j), vl(i,j+1), i=1, n)
     Else
        Write (nout, 140)(vl(i,j-1), -vl(i,j), i=1, n)
     End If
        
  Else
     Write (nout, *)
     Write (nout, 150) 'Failure in DGEEV.  INFO = ', info
  End If

  Write (nout, *)
  vector_sum = 0
  mean_occupancy = dot_product(states(1:n,1),vl(1:n,j))
  vector_sum = vector_sum + mean_occupancy
  write (nout, '(a, f5.3)') 'Mean occupancy of state (u=1,f=1): ', mean_occupancy
  mean_occupancy = dot_product(states(1:n,2),vl(1:n,j))
  vector_sum = vector_sum + mean_occupancy
  write (nout, '(a, f5.3)') 'Mean occupancy of state (u=1,f=0): ', mean_occupancy
  mean_occupancy = dot_product(states(1:n,3),vl(1:n,j))
  vector_sum = vector_sum + mean_occupancy
  write (nout, '(a, f5.3)') 'Mean occupancy of state (u=0,f=1): ', mean_occupancy
  mean_occupancy = dot_product(states(1:n,4),vl(1:n,j))
  vector_sum = vector_sum + mean_occupancy
  write (nout, '(a, f5.3)') 'Mean occupancy of state (u=0,f=0): ', mean_occupancy
  write (nout, '(a, f5.3)') 'Sum of these occupancies: ', vector_sum
  
100 Format (1X, A, I2, A, 1P, E11.4)
110 Format (1X, A, I2, A, '(', 1P, E11.4, ',', 1P, E11.4, ')')
120 Format (1X, A, I2, A)
130 Format (1X, 1P, E11.4)
140 Format (1X, '(', 1P, E11.4, ',', 1P, E11.4, ')')
150 Format (1X, A, I4)
End Program  quasi_stationary_dist
