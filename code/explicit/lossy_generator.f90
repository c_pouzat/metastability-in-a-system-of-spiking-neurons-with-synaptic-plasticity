program lossy_generator
  use iso_fortran_env, only: int32, real64, &
        stdin => input_unit, &
        stdout => output_unit, &
        stderr => error_unit
  implicit none
  integer(kind=int32) :: nb_neurons ! Number of neurons
  real(kind=real64) :: beta          ! Parameter beta
  real(kind=real64) :: lambda        ! Paramter lambda
  integer(kind=int32), parameter :: theta=1 ! threshold
  integer(kind=int32) :: nb_states ! Number of states
  integer(kind=int32) :: nb_conf ! Number of configurations
  ! Next the configurations list
  integer(kind=int32), dimension(:), allocatable :: configurations 
  logical, dimension(:), allocatable :: A_set, Rprime_set ! is a conf in A or R'
  ! Next the infinitesimal generator
  real(kind=real64), dimension(:,:), allocatable :: connection 
  integer(kind=int32), dimension(4) :: tuple
  
  read_from_cli: block
    character(len=10)    :: arg
    call get_command_argument(1, arg)
    read (arg, *) nb_neurons
    if (nb_neurons <= 0) stop &
         "The number of neurons should be > 0."    
    if (nb_neurons > 9) stop &
         "The number of neurons should be < 10."
    call get_command_argument(2, arg)
    read (arg, *) beta
    if (beta <=0) stop &
         "Parameter beta should be > 0."
    call get_command_argument(3, arg)
    read (arg, *) lambda
    if (lambda <=0) stop &
         "Parameter lambda should be > 0."
  end block read_from_cli

  nb_states = 2 * (theta + 1)
    
  nb_conf = 1
  get_nb_conf: block ! get the number of configurations
    integer :: loop_idx
    integer :: upper, lower
    integer :: numerator, denominator
    upper = nb_neurons + nb_states - 1
    lower = nb_states - 1
    numerator = 1
    do loop_idx = upper, upper - lower + 1, -1
       numerator = numerator * loop_idx
    end do
    denominator = 1
    do loop_idx = lower, 1, -1
       denominator = denominator * loop_idx
    end do
    nb_conf = numerator / denominator
  end block get_nb_conf 
  
  array_allocation: block
    integer :: allocation_status
    character(len=99) :: emsg
    ! allocate configurations array
    allocate (configurations(nb_conf), stat=allocation_status, errmsg=emsg)
    if (allocation_status > 0) then
       write (stderr,'(a)') trim(emsg)
       stop
    end if
    configurations = 0 ! initialization
    ! allocate A_set array
    allocate(A_set(nb_conf),  stat=allocation_status, errmsg=emsg)
    if (allocation_status > 0) then
       write (stderr,'(a)') trim(emsg)
       stop
    end if
    A_set = .false. ! initialization
    ! allocate Rprime_set array
    allocate(Rprime_set(nb_conf),  stat=allocation_status, errmsg=emsg)
    if (allocation_status > 0) then
       write (stderr,'(a)') trim(emsg)
       stop
    end if
    Rprime_set = .false. !initialization
    ! allocate connection array
    allocate(connection(nb_conf,nb_conf),  stat=allocation_status, errmsg=emsg)
    if (allocation_status > 0) then
       write (stderr,'(a)') trim(emsg)
       stop
    end if
    connection = 0 ! initialization
  end block array_allocation
  
  list_configurations: block
    integer :: conf_idx
    integer :: walker
    walker = nb_neurons
    conf_idx = 1
    walk_over_integers: do
       if ( conf_idx > nb_conf) exit ! no more configurations to find
       call map2conf(walker,tuple)
       if (sum(tuple) == nb_neurons) then
          if (sum(tuple(3:4)) == 0) Rprime_set(conf_idx) = .true.
          if (tuple(1) == 0) A_set(conf_idx) = .true.
          if (sum(tuple(1:3)) .le. 1) A_set(conf_idx) = .true.
          configurations(conf_idx) = walker
          conf_idx = conf_idx + 1
       end if
       walker = walker + 1
    end do walk_over_integers
  end block list_configurations

  find_connections: block
    integer(kind=int32) :: i,j, nb
    integer(kind=int32), dimension(4) :: destination
    real(kind=real64) :: row_sum
    do i = 1, nb_conf
       call map2conf(configurations(i),tuple)
       susceptible_facilitated: if (tuple(1) > 0) then
          ! deal with inactivation of synaptic facilitation
          destination = tuple
          destination(1) = tuple(1) - 1
          destination(2) = tuple(2) + 1
          nb = map2int(destination)
          j = findloc(configurations, nb, dim=1)
          connection(i,j) = tuple(1) * lambda
          ! deal with spike
          destination(1) = tuple(1) - 1 + tuple(3)
          destination(2) = tuple(2) + tuple(4)
          destination(3) = 1
          destination(4) = 0
          nb = map2int(destination)
          j = findloc(configurations, nb, dim=1)
          connection(i,j) = tuple(1) * beta
       end if susceptible_facilitated
       susceptible_unfacilitated: if (tuple(2) > 0) then
          ! deal with spike
          destination = tuple
          destination(2) = tuple(2) - 1
          destination(3) = tuple(3) + 1
          nb = map2int(destination)
          j = findloc(configurations, nb, dim=1)
          connection(i,j) = tuple(2) * beta
       end if susceptible_unfacilitated
       below_theta_facilitated: if (tuple(3) > 0) then
          ! deal with inactivation of synaptic facilitation
          destination = tuple
          destination(3) = tuple(3) - 1
          destination(4) = tuple(4) + 1
          nb = map2int(destination)
          j = findloc(configurations, nb, dim=1)
          connection(i,j) = tuple(3) * lambda
       end if below_theta_facilitated
    end do
    do i = 1, nb_conf
       row_sum = 0.0
       do j = 1, nb_conf
          if (j /= i) then
             row_sum = row_sum + connection(i,j)
          end if
       end do
       connection(i,i) = -row_sum
    end do
  end block find_connections
  
  call write_lossy_generator(configurations, A_set, Rprime_set,&
       connection, beta, lambda)
  
contains

  subroutine map2conf(number,tuple)
    integer, intent(in) :: number
    integer, dimension(4), intent(out) :: tuple
    integer :: units, tens, hundreds, thousands
    thousands = number / 1000
    hundreds = (number - thousands * 1000) / 100
    tens = (number - thousands * 1000 - hundreds * 100) / 10
    units = (number - thousands * 1000 - hundreds * 100 - tens * 10)
    tuple = [thousands, hundreds, tens, units]
  end subroutine map2conf

  function map2int(tuple) result(number)
    integer, dimension(4), intent(in) :: tuple
    integer :: number
    number = 1000 * tuple(1) + 100 * tuple(2) + 10 * tuple(3) + tuple(4)
  end function map2int
  
  subroutine write_lossy_generator(configurations, A_set, Rprime_set, &
       connection,beta, lambda)
    integer(kind=int32), dimension(:), intent(in) :: configurations
    logical, dimension(:), intent(in) :: A_set, Rprime_set
    real(kind=real64), dimension(:,:), intent(in) :: connection
    real(kind=real64), intent(in) :: beta, lambda
    integer(kind=int32), dimension(4) :: tuple
    integer(kind=int32) :: conf_idx, dest, number, from, to, n_r_star
    real(kind=real64), dimension(:,:), allocatable :: lossy_g    ! generator restricted to R*
    integer(kind=int32), dimension(:), allocatable :: r_star ! configurations restricted to R*
    ! Find out the number of states in r_star
    from = lbound(configurations, dim=1)
    to = ubound(configurations, dim=1)
    n_r_star = 0
    nb_states_in_r_star: do conf_idx = from, to
       if (.not. Rprime_set(conf_idx) .and. .not. A_set(conf_idx)) then
          n_r_star = n_r_star + 1
       end If
    end do nb_states_in_r_star
    ! Allocate lossy_g and r_star
    r_star_allocation: block
      integer :: allocation_status
      character(len=99) :: emsg
      allocate(r_star(n_r_star),  stat=allocation_status, errmsg=emsg)
      if (allocation_status > 0) then
         write (stderr,'(a)') trim(emsg)
         stop
      end if
      r_star = 0
      allocate(lossy_g(n_r_star,n_r_star),  stat=allocation_status, errmsg=emsg)
      if (allocation_status > 0) then
         write (stderr,'(a)') trim(emsg)
         stop
      end if
      lossy_g = 0.0 ! initialization
    end block r_star_allocation  
    ! lossy_g and r_star initialization
    r_star_initialization: block
      integer(kind=int32) :: row_idx, col_idx, idx
      row_idx = 0
      populate_r_star: do conf_idx = from, to
         if (.not. Rprime_set(conf_idx) .and. .not. A_set(conf_idx)) then
            row_idx = row_idx + 1
            r_star(row_idx) = configurations(conf_idx)
            col_idx = 0
            do idx = from, to
               if (.not. Rprime_set(idx) .and. .not. A_set(idx)) then
                  col_idx = col_idx + 1
                  lossy_g(row_idx,col_idx) = connection(conf_idx,idx)
               end if
            end do
         end if
      end do populate_r_star
    end block r_star_initialization
    ! Write short header
    call map2conf(r_star(1), tuple)
    write (stdout,"(a,i3,a,f5.2,a,f5.2)") "# ", sum(tuple),&
         " neurons, theta = 1, beta = ", beta, " lambda = ", lambda
    write (stdout,*) ""
    write (stdout,*) n_r_star, " : number of states_in_r_star"
    write (stdout,*) ""  
    ! Write lossy_g and r_star to output
    ! write lossy_g
    do conf_idx = 1, n_r_star
       write (stdout,"(*(f8.3))") lossy_g(conf_idx,1:n_r_star)
    end do
    write (stdout,*) ""
    write (stdout,*) ""
    ! write r_star
    do conf_idx = 1, n_r_star
       call map2conf(r_star(conf_idx), tuple)
       write (stdout,"(4(i4))") tuple 
    end do
    write (stdout,*) ""
  end subroutine write_lossy_generator
    
end program lossy_generator
