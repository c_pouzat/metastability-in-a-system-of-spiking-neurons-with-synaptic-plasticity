def map2conf(number):
    thousands = number // 1000
    hundreds = (number - thousands * 1000) // 100
    tens = (number - thousands * 1000 - hundreds * 100) // 10
    units = number - thousands * 1000 - hundreds * 100 - tens * 10
    return thousands, hundreds, tens, units

def map2int(tpl):
    return 1000*tpl[0] + 100*tpl[1] + 10*tpl[2] + tpl[3]

def list_conf(nb_neurons, theta = 1):
    """Exhaustive configuration list and more.

    Parameters
    ----------
    nb_neurons: number of neurons in the network
    theta: spiking threshold

    Returns
    --------
    A tuple of 3 elements, every element is a list with as many elements
    as there are configurations. These 3 elements are:
    conf: a listwhose elements are tuples
          (conf_idx,integer_representation,quadruple_representation),
          where conf_idx is an integer between 0 and the number of conf. -1.
    A: a Boolean whose elements are 'True' when the corresponding configurations
       are member of set A.
    Rprime: a Boolean whose elements are 'True' when the corresponding configurations
       are member of set R'.
    """
    from math import comb
    nb_states = 2*(theta+1)
    nb_conf = comb(nb_neurons+nb_states-1,nb_states-1)
    conf = []
    A = [False]*nb_conf
    Rprime = [False]*nb_conf
    walker = nb_neurons
    conf_idx = 1
    while conf_idx <= nb_conf:
        tpl = map2conf(walker)
        if sum(tpl) == nb_neurons:
            if sum(tpl[2:]) == 0:
                Rprime[conf_idx-1] = True
            if tpl[0] == 0:
                A[conf_idx-1] = True
            if sum(tpl[:3]) <= 1:
                A[conf_idx-1] = True
            conf.append((conf_idx,walker,tpl))
            conf_idx += 1
        walker += 1
    return conf, A, Rprime

def get_connexion(conf,beta=10,lmbda=4):
    """Computes the infinitesimal generator.

    Parameters
    ----------
    conf: the first element returned by 'list_conf'
    beta: value of beta (> 0)
    lmbda: value of lambda (>=0)

    Returns
    --------
    A triplet:
    - the numerical version of the infinitesimal generator as a list of lists
    - the textual representation (the diagonal elements are replaced by a
      buller symbol in order to save space on the printed version.
    - the textual representation of the diagonal of the generator (because of
      the bullet used in the former).
    """
    nb_conf = len(conf)
    lambda_factor = [[0]*nb_conf for i in range(nb_conf)]
    beta_factor = [[0]*nb_conf for i in range(nb_conf)]
    connexion_txt = [['.' for i in range(nb_conf)] for j in range(nb_conf)]
    connexion_num = [[0]*nb_conf for i in range(nb_conf)]
    conf_numbers = [elt[1] for elt in conf]
    diag_txt = [' ']*nb_conf
    dest = [0]*4
    for i in range(nb_conf):
        tpl = conf[i][-1]
        if tpl[0] > 0: # susceptible facilitated
            # deal with inactivation of synaptic facilitation
            dest[0] = tpl[0] - 1 
            dest[1] = tpl[1] + 1
            dest[2:] = tpl[2:]
            nb = map2int(dest)
            j = conf_numbers.index(nb)
            connexion_txt[i][j] = '\\textcolor{orange}{' + str(tpl[0]) + '}'
            connexion_num[i][j] = tpl[0] * lmbda
            lambda_factor[i][j] = tpl[0]
            # deal with spike
            dest[0] = tpl[0] - 1 + tpl[2]
            dest[1] = tpl[1] + tpl[3]
            dest[2] = 1
            dest[3] = 0
            nb = map2int(dest)
            j = conf_numbers.index(nb)
            connexion_txt[i][j] = '\\textcolor{blue}{' + str(tpl[0]) + '}'
            connexion_num[i][j] = tpl[0]*beta
            beta_factor[i][j] = tpl[0]
        if tpl[1] > 0: # susceptible unfacilitated
            # deal with spike
            dest[0] = tpl[0]
            dest[1] = tpl[1] - 1
            dest[2] = tpl[2] + 1
            dest[3] = tpl[3]
            nb = map2int(dest)
            j = conf_numbers.index(nb)
            connexion_txt[i][j] = '\\textcolor{blue}{' + str(tpl[1]) + '}'
            connexion_num[i][j] = tpl[1]*beta
            beta_factor[i][j] = tpl[1]
        if tpl[2] > 0: # unsusceptible facilitated
            # deal with inactivation of synaptic facilitation
            dest[0] = tpl[0] 
            dest[1] = tpl[1]
            dest[2] = tpl[2] - 1
            dest[3] = tpl[3] + 1
            nb = map2int(dest)
            j = conf_numbers.index(nb)
            connexion_txt[i][j] = '\\textcolor{orange}{' + str(tpl[2]) + '}'
            connexion_num[i][j] = tpl[2] * lmbda
            lambda_factor[i][j] = tpl[2]
    
    for i in range(nb_conf):
        l_sum = 0
        b_sum = 0
        for j in range(nb_conf):
            if j != i:
                l_sum += lambda_factor[i][j]
                b_sum += beta_factor[i][j]
        if l_sum > 0:
            if l_sum > 1:
                diag_txt[i] = '-\\textcolor{orange}{'+str(l_sum) + '\\lambda}'
            else:
                diag_txt[i] = '-\\textcolor{orange}{\\lambda}'
            if b_sum > 0:
                if b_sum > 1:
                    diag_txt[i] += ' -\\textcolor{blue}{'+str(b_sum) + '\\beta}'
                else:
                    diag_txt[i] += ' -\\textcolor{blue}{\\beta}'
        else:
            if b_sum > 0:
                if b_sum > 1:
                    diag_txt[i] = '-\\textcolor{blue}{'+str(b_sum) + '\\beta}'
                else:
                    diag_txt[i] = '-\\textcolor{blue}{\\beta}'
        connexion_txt[i][i] = '$\\bullet$'
        connexion_num[i][i] = -(l_sum * lmbda + b_sum * beta)
            
    return connexion_num, connexion_txt, diag_txt

def lossy(nb_neurons, theta = 1, beta=10, lmbda=4):
    """Computes the infinitesimal generator restricted to the quasi-
    stationary distribution support.

    Parameters
    -----------
    nb_neurons: number of neurons in the network
    theta: spiking threshold
    beta: beta value
    lmbda: lambda value

    Returns
    -------
    A quadruple:
    - A list with the indices of the configurations that are in set R*
    - a numerical representation of the lossy generator (list of list)
    - a textual representation of the lossy generator (list of list)
    - a textual representation of the diagonal
    """
    conf, A, Rprime = list_conf(nb_neurons, theta)
    connexion_num, connexion_txt, diag_txt = get_connexion(conf,beta, lmbda)
    nb_conf = len(conf)
    n_r_star = 0
    r_star = []
    for idx in range(nb_conf):
        if (not Rprime[idx]) and (not A[idx]):
            r_star.append(conf[idx][1])
            n_r_star += 1
    print('There are ' + str(n_r_star) + ' states in R*.')
    lossy_num = [[0]*n_r_star for i in range(n_r_star)]
    lossy_txt = [['.' for i in range(n_r_star)] for j in range(n_r_star)]
    lossy_diag = [' ']*n_r_star
    row_idx = -1
    for conf_idx in range(nb_conf):
        if (not Rprime[conf_idx]) and (not A[conf_idx]):
            row_idx += 1
            col_idx = -1
            for idx in range(nb_conf):
                if (not Rprime[idx]) and (not A[idx]):
                   col_idx += 1
                   lossy_num[row_idx][col_idx] = connexion_num[conf_idx][idx]
                   lossy_txt[row_idx][col_idx] = connexion_txt[conf_idx][idx]
                   if conf_idx == idx:
                       lossy_diag[row_idx] = diag_txt[conf_idx]
    return r_star, lossy_num, lossy_txt, lossy_diag

def conf2dot(nb_neurons, theta = 1):
    conf, A, Rprime = list_conf(nb_neurons, theta)
    nb_conf = len(conf)
    connexion = [[0]*nb_conf for i in range(nb_conf)]
    conf_numbers = [elt[1] for elt in conf]
    dest = [0]*4
    for i in range(nb_conf):
        tpl = conf[i][-1]
        if tpl[0] > 0: # susceptible facilitated
            # deal with inactivation of synaptic facilitation
            dest[0] = tpl[0] - 1 
            dest[1] = tpl[1] + 1
            dest[2:] = tpl[2:]
            nb = map2int(dest)
            j = conf_numbers.index(nb)
            connexion[i][j] = 1
            # deal with spike
            dest[0] = tpl[0] - 1 + tpl[2]
            dest[1] = tpl[1] + tpl[3]
            dest[2] = 1
            dest[3] = 0
            nb = map2int(dest)
            j = conf_numbers.index(nb)
            connexion[i][j] = 2
        if tpl[1] > 0: # susceptible unfacilitated
            # deal with spike
            dest[0] = tpl[0]
            dest[1] = tpl[1] - 1
            dest[2] = tpl[2] + 1
            dest[3] = tpl[3]
            nb = map2int(dest)
            j = conf_numbers.index(nb)
            connexion[i][j] = 2
        if tpl[2] > 0: # unsusceptible facilitated
            # deal with inactivation of synaptic facilitation
            dest[0] = tpl[0] 
            dest[1] = tpl[1]
            dest[2] = tpl[2] - 1
            dest[3] = tpl[3] + 1
            nb = map2int(dest)
            j = conf_numbers.index(nb)
            connexion[i][j] = 1
    
    res = 'digraph ntw{\n'
    res += 'layout=neato\n'
    res += 'graph [epsilon=0.0001,overlap="false",splines="true"];\n'
    res += 'node [shape=record]\n'
    n_r_star = 0
    for conf_idx in range(nb_conf):
        if A[conf_idx] or not Rprime[conf_idx]: # not in R prime
            res += str(conf_idx)
            if A[conf_idx]:
                res += ' [style=filled fillcolor=grey label="{'
            else:
                if not Rprime[conf_idx]:
                    n_r_star += 1
                    res += ' [style=filled fillcolor=lightblue label="{ ' +\
                        str(n_r_star) + ' } | {'
            tpl = conf[conf_idx][-1]
            res += str(tpl[0]) + '|' + str(tpl[2]) + '}|{' + \
                    str(tpl[1]) + '|' + str(tpl[3]) + '}"];\n'
    for conf_idx in range(nb_conf):
        if Rprime[conf_idx]: # in R prime
            res += str(conf_idx)
            res += ' [label="{'
            tpl = conf[conf_idx][-1]
            res += str(tpl[0]) + '|' + str(tpl[2]) + '}|{' + \
                str(tpl[1]) + '|' + str(tpl[3]) + '}"];\n'
    for conf_idx in range(nb_conf):
        for dest in range(nb_conf):
            if connexion[conf_idx][dest] == 1: # facilitation loss
                res += str(conf_idx) + ' -> ' + str(dest) + ' [penwidth=1];\n'
            if connexion[conf_idx][dest] == 2: # spike
                res += str(conf_idx) + ' -> ' + str(dest) + ' [penwidth=3];\n'
    res += '}\n'
    
    return res

def connexion2tex(connexion_txt):
    nb_conf = len(connexion_txt)
    res = '\\tiny\n'
    res += '\\begin{tabular}{ccccccccccccccccccccccccccccc}\n'
    for i in range(nb_conf):
        res += ' & '.join(connexion_txt[i]) + ' \\\\ \n'
    res += '\\end{tabular}\n'
    res += '\\normalsize\n'
    return res

def diag2tex(diag_text):
    ng=3
    nb = len(diag_text)
    nr = 10
    hd1 = '\\begin{tabular}{' + ' | '.join(['cc']*ng) + '}\n'
    res = '\\small\n'
    res += hd1
    hd2 = ' & '.join(['index & rate']*ng) + '\\\\ \\hline \n'
    res += hd2
    for r_idx in range(nr-1):
        seq = [str(r_idx + (i*nr) + 1) + ' & ' + diag_text[r_idx + (i*nr)]
               for i in range(ng)]
        res += ' & '.join(seq) + '\\\\ \n'
    seq = [str(10) + ' & ' + diag_text[9], str(20) + ' & ' + diag_text[19], ' & ']
    res += ' & '.join(seq) + '\\\\ \n'
    res += '\\end{tabular}\n'
    res += '\\normalsize\n'
    return res

def get_qsd(lossy_num):
    """Returns the residence time and the QSD.

    Parameters
    ----------
    lossy_m: a list of lists (numerical lossy generator returned by 'lossy')

    Returns:
    A tuple with the residence time and the quasi-stationary distribution
    """
    import numpy as np
    lossy_m = np.array(lossy_num)
    eigenvalues, eigenvectors = np.linalg.eig(lossy_m.T)
    tau = -np.real(eigenvalues[0])
    egv = np.real(eigenvectors[:,0])
    egv = egv / np.sum(egv)
    return 1/tau, egv
    
def occupancy(r_star, qsd):
    """Expected occupancy of the 4 states under the QSD.

    Parameters
    ----------
    r_star: first list returned by 'lossy'
    qsd: second element returned by 'get_qsd'

    Returns
    -------
    A tuple whose elements are:
    - the expected number of neurons in state: suceptible and facilitated
    - the expected number of neurons in state: suceptible and unfacilitated
    - the expected number of neurons in state: not suceptible and facilitated
    - the expected number of neurons in state: not suceptible and unfacilitated
    """
    tpls = [map2conf(r) for r in r_star]
    n1 = sum([tpls[i][0]*qsd[i] for i in range(len(qsd))])
    n2 = sum([tpls[i][1]*qsd[i] for i in range(len(qsd))])
    n3 = sum([tpls[i][2]*qsd[i] for i in range(len(qsd))])
    n4 = sum([tpls[i][3]*qsd[i] for i in range(len(qsd))])
    return n1, n2, n3, n4

def qsd_all_in_one(beta, lmbda, nb_neurons = 5):
    """Computes the QSD, the mean survival time in R' and the
    expected number of neurons in each of the 4 states under the QSD.

    The program prints to the stdout the results.
    
    Parameters
    ----------
    beta: spiking rate in a suceptible state
    lmbda: synaptic facilitation loss rate
    nb_neurons: the number of neurons (< 10)
    
    Returns
    -------
    A tuple with:
    - the mean survival time
    - the QSD (a numpy array)
    - a tuple with the expected values of:
      - suceptible facilited
      - suceptible unfacilited
      - below threshold facilitated
      - below threshold unfacilitated
    """
    r_star, lossy_num, _, _ = lossy(nb_neurons, theta = 1, beta=beta, lmbda=lmbda)
    st, qsd = get_qsd(lossy_num)
    occup = occupancy(r_star,qsd)
    print('Network with ' + str(nb_neurons) + ' neurons, beta = ' +
          str(beta) + ', lambda = ' + str(lmbda))
    print('Mean QSD survival time: ' + str(round(st,3)))
    print(' ')
    print('QSD:')
    for i in range(len(qsd)):
        print('Prob. of state ('+str(r_star[i])+'): ' + str(round(qsd[i],3)))
    print(' ')
    print('Expected number of neurons in state (u=1,f=1): ' + str(round(occup[0],3)))
    print('Expected number of neurons in state (u=1,f=0): ' + str(round(occup[1],3)))
    print('Expected number of neurons in state (u=0,f=1): ' + str(round(occup[2],3)))
    print('Expected number of neurons in state (u=0,f=0): ' + str(round(occup[3],3)))
    print('Sum of these expectations: ' + str(round(sum(occup),3)))
    return st, qsd, occup

          

if __name__ == '__main__':
    nb_neuron = 5
    beta = 10
    lmbda = 5
    dir4figs = 'figs'
    name1 = dir4figs +'/n'+str(nb_neuron)+'t1.dot'
    with open(name1,'w') as f:
        f.write(conf2dot(nb_neuron))
    r_star, lossy_n, lossy_t, lossy_d = lossy(nb_neuron,beta=beta,lmbda=lmbda)
    n2 = 'n'+str(nb_neuron)+'t1b'+str(beta)+'l'+str(lmbda)+'.tex'
    n3 = 'n'+str(nb_neuron)+'t1b'+str(beta)+'l'+str(lmbda)+'_diag.tex'
    with open(n2,'w') as f:
        f.write(connexion2tex(lossy_t))
    with open(n3,'w') as f:
        f.write(diag2tex(lossy_d))
    n5b10l5 = qsd_all_in_one(beta,lmbda,nb_neuron)
