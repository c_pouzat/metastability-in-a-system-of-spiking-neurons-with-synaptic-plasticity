set terminal pngcairo size 640,480
set output "figs/ntw_stp_py_test_1_fig_2.png"
set title 'Membrane potential at 0 or 4'
set grid
set key at 70,1.25 top left
set xlabel 'Time'
set ylabel 'Mean nb of neurons'
set bars small
plot [] [0:2] 'simulations/test_python' index 1000 u 1:4:16 \
     w yerrorlines pt 0 lc 'black' lw 2 title '(u,f)=(0,1)',\
     'simulations/test_python' index 1000 u 1:3:15 \
     w yerrorlines pt 0 lc 'red' lw 2 title '(u,f)=(0,0)',\
     'simulations/test_python' index 1000 u 1:12:24 \
     w yerrorlines pt 0 lc 'blue' lw 2 title '(u,f)=(4,1)',\
     'simulations/test_python' index 1000 u 1:11:23 \
     w yerrorlines pt 0 lc 'orange' lw 2 title '(u,f)=(4,0)'
