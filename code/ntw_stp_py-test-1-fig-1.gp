set terminal pngcairo size 640,480
set output "figs/ntw_stp_py_test_1_fig_1.png"
set title 'Membrane potential at least θ'
set grid
set key
set xlabel 'Time'
set ylabel 'Mean nb of neurons'
set bars small
plot [] [15:25] 'simulations/test_python' index 1000 u 1:14:26 \
     w yerrorlines pt 0 lc 'black' lw 2 title 'f=1',\
     'simulations/test_python' index 1000 u 1:13:25 \
     w yerrorlines pt 0 lc 'red' lw 2 title 'f=0'
