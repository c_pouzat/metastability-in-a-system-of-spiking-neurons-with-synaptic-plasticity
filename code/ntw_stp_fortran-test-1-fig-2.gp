set terminal pngcairo size 640,480
set output "figs/ntw_stp_fortran_test_1_fig_2.png"
set title 'Number of replicates still alive'
set grid
unset key
set xlabel 'Time'
set ylabel 'Number of replicates'
plot [] [4000:10000] 'test_fortran_2' index 10000 u 1:2 with lines lc 'black' lw 2
