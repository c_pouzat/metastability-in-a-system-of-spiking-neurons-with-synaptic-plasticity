set terminal pngcairo size 640,520
set output "figs/ntw_stp_fortran_test_4_fig_2.png"
set title 'Membrane potential at least θ'
set grid
set key at 0.4,450 top left spacing 2
set xlabel 'Time'
set ylabel 'Mean nb of neurons'
set bars small
plot [] [] 'simulations/test_fortran_3' index 10000 u 1:(10*$14) \
     w lines pt 0 lc 'orange' lw 3 title 'x10 (u,f)=(θ,1) N=50 θ=5',\
     'simulations/test_fortran_N500_short' index 10000 u 1:104:(2*$206) \
     w yerrorlines pt 0 lc 'black' lw 2 title '    (u,f)=(θ,1) N=500 θ=50',\
     'simulations/test_fortran_3' index 10000 u 1:(10*$13) \
     w lines pt 0 lc 'blue' lw 3 title 'x10 (u,f)=(θ,0) N=50 θ=5',\
     'simulations/test_fortran_N500_short' index 10000 u 1:103:(2*$205) \
     w yerrorlines pt 0 lc 'red' lw 2 title '    (u,f)=(θ,0) N=500 θ=50'
