set terminal pngcairo size 640,540
set output "figs/ecdf_u5_test_fortran_2.png"
set title 'Membrane potential at least θ'
set grid
unset key 
set xlabel 'Number of neurons'
set ylabel 'ECDF'
plot for [col=3:22] 'simulations/test_fortran_2_u5_f0' \
     using 1:col with steps lw 0.1 lc 'red',\
     for [col=3:22] 'simulations/test_fortran_2_u5_f1' \
     using 1:col with steps lw 0.1 lc 'black'
