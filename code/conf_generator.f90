program conf_generator
  use iso_fortran_env, only: int32, real64, &
        stdin => input_unit, &
        stdout => output_unit, &
        stderr => error_unit
  implicit none
  integer :: nb_neurons ! Number of neurons
  integer, parameter :: theta=1 ! threshold
  integer :: nb_states ! Number of states
  integer :: nb_conf ! Number of configurations
  integer, dimension(:), allocatable :: configurations ! the configurations list
  logical, dimension(:), allocatable :: A_set, Rprime_set ! is a conf in A or R'
  integer, dimension(:,:), allocatable :: connection ! the connections between conf
  integer, dimension(4) :: tuple
  
  read_from_cli: block
    character(len=10)    :: arg
    call get_command_argument(1, arg)
    read (arg, *) nb_neurons
    if (nb_neurons <= 0) stop &
         "The number of neurons should be > 0."    
    if (nb_neurons > 9) stop &
         "The number of neurons should be < 10."    
  end block read_from_cli

  nb_states = 2 * (theta + 1)
    
  nb_conf = 1
  get_nb_conf: block ! get the number of configurations
    integer :: loop_idx
    integer :: upper, lower
    integer :: numerator, denominator
    upper = nb_neurons + nb_states - 1
    lower = nb_states - 1
    numerator = 1
    do loop_idx = upper, upper - lower + 1, -1
       numerator = numerator * loop_idx
    end do
    denominator = 1
    do loop_idx = lower, 1, -1
       denominator = denominator * loop_idx
    end do
    nb_conf = numerator / denominator
  end block get_nb_conf 
  
  array_allocation: block
    integer :: allocation_status
    character(len=99) :: emsg
    ! allocate configurations array
    allocate (configurations(nb_conf), stat=allocation_status, errmsg=emsg)
    if (allocation_status > 0) then
       write (stderr,'(a)') trim(emsg)
       stop
    end if
    configurations = 0 ! initialization
    ! allocate A_set array
    allocate(A_set(nb_conf),  stat=allocation_status, errmsg=emsg)
    if (allocation_status > 0) then
       write (stderr,'(a)') trim(emsg)
       stop
    end if
    A_set = .false. ! initialization
    ! allocate Rprime_set array
    allocate(Rprime_set(nb_conf),  stat=allocation_status, errmsg=emsg)
    if (allocation_status > 0) then
       write (stderr,'(a)') trim(emsg)
       stop
    end if
    Rprime_set = .false. !initialization
    ! allocate connection array
    allocate(connection(nb_conf,nb_conf),  stat=allocation_status, errmsg=emsg)
    if (allocation_status > 0) then
       write (stderr,'(a)') trim(emsg)
       stop
    end if
    connection = 0 ! initialization
  end block array_allocation
  
  list_configurations: block
    integer :: conf_idx
    integer :: walker
    walker = nb_neurons
    conf_idx = 1
    walk_over_integers: do
       if ( conf_idx > nb_conf) exit ! no more configurations to find
       call map2conf(walker,tuple)
       if (sum(tuple) == nb_neurons) then
          if (sum(tuple(3:4)) == 0) Rprime_set(conf_idx) = .true.
          if (tuple(1) == 0) A_set(conf_idx) = .true.
          if (sum(tuple(1:3)) .le. 1) A_set(conf_idx) = .true.
          configurations(conf_idx) = walker
          conf_idx = conf_idx + 1
       end if
       walker = walker + 1
    end do walk_over_integers
  end block list_configurations

  find_connections: block
    integer :: i,j, nb
    integer, dimension(4) :: destination
    do i = 1, nb_conf
       call map2conf(configurations(i),tuple)
       susceptible_facilitated: if (tuple(1) > 0) then
          ! deal with unactivation
          destination = tuple
          destination(1) = tuple(1) - 1
          destination(2) = tuple(2) + 1
          nb = map2int(destination)
          j = findloc(configurations, nb, dim=1)
          connection(i,j) = 1
          ! deal with spike
          destination(1) = tuple(1) - 1 + tuple(3)
          destination(2) = tuple(2) + tuple(4)
          destination(3) = 1
          destination(4) = 0
          nb = map2int(destination)
          j = findloc(configurations, nb, dim=1)
          connection(i,j) = 2
       end if susceptible_facilitated
       susceptible_unfacilitated: if (tuple(2) > 0) then
          ! deal with spike
          destination = tuple
          destination(2) = tuple(2) - 1
          destination(3) = tuple(3) + 1
          nb = map2int(destination)
          j = findloc(configurations, nb, dim=1)
          connection(i,j) = 2
       end if susceptible_unfacilitated
       below_theta_facilitated: if (tuple(3) > 0) then
          ! deal with inactivation
          destination = tuple
          destination(3) = tuple(3) - 1
          destination(4) = tuple(4) + 1
          nb = map2int(destination)
          j = findloc(configurations, nb, dim=1)
          connection(i,j) = 1
       end if below_theta_facilitated
    end do
  end block find_connections
  
  call write2dot(configurations, A_set, Rprime_set, connection)
  
contains

  subroutine map2conf(number,tuple)
    integer, intent(in) :: number
    integer, dimension(4), intent(out) :: tuple
    integer :: units, tens, hundreds, thousands
    thousands = number / 1000
    hundreds = (number - thousands * 1000) / 100
    tens = (number - thousands * 1000 - hundreds * 100) / 10
    units = (number - thousands * 1000 - hundreds * 100 - tens * 10)
    tuple = [thousands, hundreds, tens, units]
  end subroutine map2conf

  function map2int(tuple) result(number)
    integer, dimension(4), intent(in) :: tuple
    integer :: number
    number = 1000 * tuple(1) + 100 * tuple(2) + 10 * tuple(3) + tuple(4)
  end function map2int
  
  subroutine write2dot(configurations, A_set, Rprime_set, connection)
    integer, dimension(:), intent(in) :: configurations
    logical, dimension(:), intent(in) :: A_set, Rprime_set
    integer, dimension(:,:), intent(in) :: connection
    integer, dimension(4) :: tuple
    integer :: conf_idx, dest, number, from, to
  
    from = lbound(configurations, dim=1)
    to = ubound(configurations, dim=1)
    write (stdout, '(a)') 'digraph ntw{'
    write (stdout, '(a)') 'layout=neato'
    write (stdout, '(a)') 'graph [epsilon=0.0001,overlap="false",splines="true"];'
    write (stdout, '(a)') 'node [shape=record]'
    ! write node list, keep members of R' set for the end 
    not_in_R_prime: do conf_idx = from, to 
       number = configurations(conf_idx)
       if (A_set(conf_idx) .or. .not. Rprime_set(conf_idx)) then
          write (stdout, '(i4)', advance='no') conf_idx
          if (A_set(conf_idx)) then
             write (stdout, '(a)', advance='no') &
                  ' [style=filled fillcolor=grey label="{'
          else if (.not. Rprime_set(conf_idx)) then
             write (stdout, '(a)', advance='no') &
                  ' [style=filled fillcolor=lightblue label="{'
          end if
          call map2conf(number, tuple)
          write (stdout,'(i1,a,i1,a,i1,a,i1,a)') &
               tuple(1), '|', tuple(3), '}|{', tuple(2), '|', tuple(4), '}"];'
       end if
    end do not_in_R_prime
    in_R_prime: do conf_idx = from, to
       number = configurations(conf_idx)
       if (Rprime_set(conf_idx)) then
          write (stdout, '(i4)', advance='no') conf_idx
          write (stdout, '(a)', advance='no') ' [label="{'
          call map2conf(number, tuple)
          write (stdout,'(i1,a,i1,a,i1,a,i1,a)') &
               tuple(1), '|', tuple(3), '}|{', tuple(2), '|', tuple(4), '}"];'
       end if
    end do in_R_prime
    ! write edges
    outer_conf_loop: do conf_idx = from, to
       inner_conf_loop: do dest = from, to
          facilitation_loss_edge: if (connection(conf_idx,dest) == 1) then
             write (stdout, '(i4,a,i4,a)') conf_idx, ' -> ', dest, &
                  ' [penwidth=1];'
          end if facilitation_loss_edge
          spike_edge: if (connection(conf_idx,dest) == 2) then
             write (stdout, '(i4,a,i4,a)') conf_idx, ' -> ', dest, &
                  ' [penwidth=3];'
          end if spike_edge
       end do inner_conf_loop
    end do outer_conf_loop
    write (stdout, '(a)') '}'
  end subroutine write2dot
    
end program conf_generator
