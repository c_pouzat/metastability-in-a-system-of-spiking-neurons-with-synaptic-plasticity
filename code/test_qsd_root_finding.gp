set terminal pngcairo size 640,540
set output "figs/test_qsd_root_finding.png"
set title 'Fixed points (N=50, β=10, λ=6, θ=5)'
set grid
unset key 
set xlabel 'Nb of neurons at (θ,1)'
set ylabel 'Nb of neurons at (θ,1)'
N = 50
β = 10.0
λ = 6.0
θ = 5
f(x) = N*β/(λ+β) * (β*x/(λ+β*x))**θ - θ
d(x) = x
set style arrow 1 nohead ls 1 lc 'orange' lw 2
set arrow from 1.65514350,0 to 1.65514350,25 as 1
set arrow from 22.3775063,0 to 22.3775063,25 as 1
plot [0:25] [0:25] f(x) lc 'blue' lw 2,\
     d(x) lc 'black' lw 2
