set terminal pngcairo size 640,540
set output "figs/ecdf_f0_u5_seq_test_fortran_2.png"
set title 'Membrane potential at least θ with a non facilitated synapse'
set grid
set palette defined (0 'blue', 0.5 'grey', 1 'red')
set cbrange [10:200]
unset key
set xlabel 'Number of neurons'
set ylabel 'ECDF'
plot [10:30] [0:1] 'test_fortran_2_f0_u5' \
     using 1:3 with steps lc palette cb 10 lw 0.3,\
     '' using 1:4 with steps lc palette cb 20 lw 0.3,\
     '' using 1:5 with steps lc palette cb 30 lw 0.3,\
     '' using 1:6 with steps lc palette cb 40 lw 0.3,\
     '' using 1:7 with steps lc palette cb 50 lw 0.3,\
     '' using 1:8 with steps lc palette cb 60 lw 0.3,\
     '' using 1:9 with steps lc palette cb 70 lw 0.3,\
     '' using 1:10 with steps lc palette cb 80 lw 0.3,\
     '' using 1:11 with steps lc palette cb 90 lw 0.3,\
     '' using 1:12 with steps lc palette cb 100 lw 0.3,\
     '' using 1:13 with steps lc palette cb 110 lw 0.3,\
     '' using 1:14 with steps lc palette cb 120 lw 0.3,\
     '' using 1:15 with steps lc palette cb 130 lw 0.3,\
     '' using 1:16 with steps lc palette cb 140 lw 0.3,\
     '' using 1:17 with steps lc palette cb 150 lw 0.3,\
     '' using 1:18 with steps lc palette cb 160 lw 0.3,\
     '' using 1:19 with steps lc palette cb 170 lw 0.3,\
     '' using 1:20 with steps lc palette cb 180 lw 0.3,\
     '' using 1:21 with steps lc palette cb 190 lw 0.3,\
     '' using 1:22 with steps lc palette cb 200 lw 0.3
