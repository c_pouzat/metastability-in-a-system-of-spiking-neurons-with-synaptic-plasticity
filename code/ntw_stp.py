#!/bin/env python3
from random import seed, random
import argparse
from math import log,exp,sqrt

# Read input parameters
script_description = ("Simulates Nrep replicates of an homogenous network\n"
                      "made of N neurons.\n"
                      "Two variables are associated with each neuron:\n"
                      "  a \"membrane potential\" U,\n"
                      "  a \"synaptic facilitation\" F.\n"
                      "The neurons spike independently of each other with a\n"
                      "rate beta once their membrane potential equals or\n"
                      "exceeds a threshold theta.\n"
                      "The neuron that spiked increases the membrane potential\n"
                      "of all the other neurons by 1 if it's synapse was\n"
                      "facilitated, O otherwise.\n"
                      "After the spike the facilitation of the neuron\n"
                      "that spiked is set to 1 (facilitated) if it was 0\n"
                      "(unfacilitated) and stays at 1 otherwise.\n"
                      "The membrane potential of the neuron that just spiked\n"
                      "is set to 0.\n"
                      "Independently of spike events the facilitation of each\n"
                      "neuron drops to 0 exponentially with rate lambda.\n")
parser = argparse.ArgumentParser(description=script_description,
                                 formatter_class=argparse.RawDescriptionHelpFormatter)
# Start with the number of neurons
def _nb_neurons(string):
    n = int(string)
    if n <= 0:
        msg = "The number of neurons must be > 0"
        raise argparse.ArgumentTypeError(msg)
    return n
parser.add_argument('-n', '--network-size',
                    type=_nb_neurons, dest='Nb',
                    help=('The network size (default 100)'),
                    default=100)
# Set the (pseudo)random number generator seed
parser.add_argument('-s', '--seed',
                    type=int, dest='seed',
                    help='The random number generator seed',
                    required=True)
# Set d
parser.add_argument('-d', '--duration',
                    type=float, dest='duration',
                    help=('the simulated duration in seconds (default 100)'),
                    default=100)
# Set l
parser.add_argument('-l', '--lambda',
                    type=float, dest='lmbda',
                    help=('synaptic facilitation decay rate (default 6.7)'),
                    default=6.7)

# Set beta
parser.add_argument('-b', '--beta',
                    type=float, dest='beta',
                    help=('spike rate for neurons above threshold (default 10)'),
                    default=10)
# Set theta
parser.add_argument('-t', '--theta',
                    type=int, dest='theta',
                    help=('membrane potential threshold for spiking (default 10)'),
                    default=10)
# Set period
parser.add_argument('-p', '--sampling-period',
                    type=int, dest='period',
                    help=('sampling period between two state storage to disk (default 1)'),
                    default=1)
# Set number of replicates
parser.add_argument('-r', '--n-replicates',
                    type=int, dest='nrep',
                    help=('number of replicates to simulate (default 1)'),
                    default=1)

args = parser.parse_args()
Nb = args.Nb
duration = args.duration
beta = args.beta
lmbda = args.lmbda
graine = args.seed  # graine is seed in French
theta = args.theta
period = args.period
nrep = args.nrep
if nrep <= 0:
    msg = "The number of replicates must be > 0"
    raise argparse.ArgumentTypeError(msg)


# Do the job!
# Do variable allocations and initializations
seed(graine)  # PRNG initialization
n_states = 2*(theta+1) # number of states to consider
Z_0 = [0]*n_states  # list for Z storage
Z_0[-1] = Nb # All neurons start active and facilitated
if period > 0 and nrep > 1:
    n_times = round(duration/period)+1 # the number of time steps
    # EZ stores the mean occupation of each state at each time step
    EZ = [[i*period]+[0]*(n_states+1) for i in range(n_times)]
    EZ[0][1] = nrep
    EZ[0][2:] = Z_0
    # SZ store the varaince of the occupation of each state at each time step
    SZ = [[i*period]+[0]*(n_states+1) for i in range(n_times)]
    SZ[0][1] = nrep

# Open result files and write their headers
if nrep > 1:
    header = ("# Simulation of a networks with {0:d} neurons\n"
              "# PRNG seed set at {1:d}\n"
              "# All neurons start in the active state.\n"
              "# All synapses start facilitated.\n"
              "# {6:d} replicates of the system are simulated.\n"
              "# Parameter beta = {2:f}\n"
              "# Parameter lambda = {3:f}\n"
              "# Parameter theta = {4:d}\n"
              "# Sampling period = {7:f}\n"
              "# Simulation duration = {5:f}\n\n")
    print(header.format(Nb, graine, beta, lmbda, theta, duration, nrep, period))
else:
    header = ("# Simulation of a networks with {0:d} neurons\n"
              "# PRNG seed set at {1:d}\n"
              "# All neurons start in the active state.\n"
              "# All synapses start facilitated.\n"
              "# Parameter beta = {2:f}\n"
              "# Parameter lambda = {3:f}\n"
              "# Parameter theta = {4:d}\n"
              "# Sampling period = {6:f}\n"
              "# Simulation duration = {5:f}\n\n")
    print(header.format(Nb, graine, beta, lmbda, theta, duration,period))

# Run the simulation
for rep_idx in range(nrep):
    if nrep > 1:
        print("# Replicate {0:d}:".format(rep_idx+1))
    Z = [Z_0[i] for i in range(len(Z_0))]
    F_dot = sum([Z[2*k+1] for k in range(theta+1)])
    t_now = 0.0
    if period > 0: # write initial state
        t_next = period
        res = [t_next-period] + Z
    else:
        res = [t_now] + Z
    print(*res)
    while t_now < duration:
        n_spiking = Z[-2] + Z[-1]
        nu = beta * n_spiking + lmbda * F_dot # global event rate
        t_now -= log(random())/nu
        if period > 0 and t_now >= t_next:
            res = [t_next] + Z
            print(*res)
            if nrep > 1:
                t_idx = round(t_next/period) 
                EZ[t_idx][1] += 1
                delta = [Z[i] - EZ[t_idx][2+i] for i in range(n_states)]
                EZ[t_idx][2:] = [EZ[t_idx][2+i] + delta[i] / EZ[t_idx][1] \
                                 for i in range(n_states)]
                delta2 = [Z[i] - EZ[t_idx][2+i] for i in range(n_states)]
                SZ[t_idx][1] += 1
                SZ[t_idx][2:] = [SZ[t_idx][2+i] + delta[i] * delta2[i] \
                                 for i in range(n_states)] 
            t_next += period
        if beta*n_spiking/nu > random(): # event is a spike
            if Z[-1]/n_spiking > random(): # the synapse is facilitated
                Z[-1] = Z[-1]-1+Z[-3]
                Z[-2] += Z[-4]
                for i in range(n_states-3,1,-1):
                    Z[i] = Z[i-2]
                Z[1] = 1
                Z[0] = 0
            else: # the synapse is not facilitated
                Z[1] += 1
                Z[-2] -= 1
                F_dot += 1
        else: # event is lost facilitation
            chance = random()
            for k in range(theta+1):
                ratio = Z[2*k+1] / F_dot
                if chance <= ratio:
                    break
                else:
                    chance -= ratio
            Z[2*k+1] -= 1
            Z[2*k] += 1
            F_dot -= 1
                
        if period == 0:
            res = [t_now] + Z
            print(*res)
        if Z[-1] == 0: # Dead network
            print("# Dead network at time {0:f}\n".format(t_now))
            t_now = duration
    if nrep > 1:
        print("# Replicate {0:d} done.\n\n".format(rep_idx+1))
if nrep > 1 and period > 0:
    from sigfig import round
    print("# Summary statistics:")
    for t_idx in range(len(SZ)):
        divisor = SZ[t_idx][1]
        SZ[t_idx][2:] = [sqrt(SZ[t_idx][2+k]/(divisor-1)/divisor)\
                         for k in range(n_states)]
        res1 = [t_idx*period] + [EZ[t_idx][1]] + \
            [float(round(EZ[t_idx][2+k],SZ[t_idx][2+k]).split()[0]) \
             for k in range(n_states)]
        res1 += [float(round(EZ[t_idx][2+k],SZ[t_idx][2+k]).split()[2]) \
             for k in range(n_states)]
        print(*res1)
