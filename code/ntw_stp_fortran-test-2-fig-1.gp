set terminal pngcairo size 640,480
set output "figs/ntw_stp_fortran_test_2_fig_1.png"
set title 'Number of replicates still alive'
set grid
unset key
set xlabel 'Time'
set ylabel 'Number of replicates'
set logscale y
plot [] [100:10000] 'simulations/test_fortran_2_long' \
     index 10000 u 1:2 with lines lc 'black' lw 2
