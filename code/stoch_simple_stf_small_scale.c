#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <getopt.h>
#include <stdbool.h>

/* Pseudo-Random Number Generator definition */
/*  Written in 2016-2018 by David Blackman and Sebastiano Vigna (vigna@acm.org)

To the extent possible under law, the author has dedicated all copyright
and related and neighboring rights to this software to the public domain
worldwide. This software is distributed without any warranty.

See <http://creativecommons.org/publicdomain/zero/1.0/>. */

#include <stdint.h>

/* This is xoroshiro128+ 1.0, our best and fastest small-state generator
   for floating-point numbers. We suggest to use its upper bits for
   floating-point generation, as it is slightly faster than
   xoroshiro128**. It passes all tests we are aware of except for the four
   lower bits, which might fail linearity tests (and just those), so if
   low linear complexity is not considered an issue (as it is usually the
   case) it can be used to generate 64-bit outputs, too; moreover, this
   generator has a very mild Hamming-weight dependency making our test
   (http://prng.di.unimi.it/hwd.php) fail after 5 TB of output; we believe
   this slight bias cannot affect any application. If you are concerned,
   use xoroshiro128** or xoshiro256+.

   We suggest to use a sign test to extract a random Boolean value, and
   right shifts to extract subsets of bits.

   The state must be seeded so that it is not everywhere zero. If you have
   a 64-bit seed, we suggest to seed a splitmix64 generator and use its
   output to fill s. 

   NOTE: the parameters (a=24, b=16, b=37) of this version give slightly
   better results in our test than the 2016 version (a=55, b=14, c=36).
*/

static inline uint64_t rotl(const uint64_t x, int k) {
	return (x << k) | (x >> (64 - k));
}


static uint64_t s[2];

uint64_t next(void) {
	const uint64_t s0 = s[0];
	uint64_t s1 = s[1];
	const uint64_t result = s0 + s1;

	s1 ^= s0;
	s[0] = rotl(s0, 24) ^ s1 ^ (s1 << 16); // a, b
	s[1] = rotl(s1, 37); // c

	return result;
}

double unif(void) {
    return (next() >> 11) * (1. / (UINT64_C(1) << 53));
}


/* Function run_sim does "the real work" */
int run_sim(int N, // Network size
	    uint64_t seed1, // First part of the PRNG seed
	    uint64_t seed2, // Second part of the PRNG seed 
	    double p_f_0, // Prob. for a synapse to be facilitated, used for initialization
	    unsigned int u_0_max, // Max value of the unif. dist. used for U initialization
	    double theta, // Threshold for entering in the "spiking" state    
	    double beta, // Potential to rate conversion factor
	    double lambda, // inverse time cst of R decay
	    double duration, // simulation duration
	    char * out_ntw, // name of first result file
	    char * out_neuron // name of second result file
  ) {

  s[0] = seed1;
  s[1] = seed2;
  unsigned int * u = malloc(N*sizeof(unsigned int)); // Membrane potential process
  _Bool * f = malloc(N*sizeof(_Bool)); // State of the synapse
  unsigned int n_ceil_theta = 0; // The number of neurons above threshold
  unsigned int f_sum = 0; // The sum of the active synapses over the network
  for (size_t i=0; i<N; i++) {
    u[i]=(int) floor(u_0_max*unif());
    if (u[i] >= theta)
      n_ceil_theta += 1;  
    if (unif() < p_f_0) {
      f[i]=true;
      f_sum += 1;
    } else {
      f[i]=false;
    }
  } 
  double t_now=0.0; // time
  int n_total = 0; // total number of spikes up to t_now  

  FILE *fout_ntw=fopen(out_ntw,"w");
  FILE *fout_neuron=fopen(out_neuron,"w");
  
  fprintf(fout_ntw,"# Simulation of a networks with %d neurons\n"
  	"# Xoroshiro128+ PRNG seeds set at %d and %d\n"
  	"# The initial max membrane potential was set to %d\n"
  	"# The initial probability for a synapse to be active was set to %f\n"
  	"# Parameter theta = %f\n"    
  	"# Parameter beta = %f\n"
  	"# Parameter lambda = %f\n"
  	"# Simulation duration = %f\n\n",
  	N, (int) seed1,(int) seed2, (int) u_0_max, p_f_0, theta, beta, lambda, duration);
  
  fprintf(fout_ntw,"# Spike time  Total nb of spikes  Neuron of origin  "
  	"Neurons >= theta  N synapse active  f=1 at spike\n");
  
  fprintf(fout_neuron,"# Simulation of a networks with %d neurons\n"
  	"# Xoroshiro128+ PRNG seeds set at %d and %d\n"
  	"# The initial max membrane potential was set to %d\n"
  	"# The initial probability for a synapse to be active was set to %f\n"
  	"# Parameter theta = %f\n"    
  	"# Parameter beta = %f\n"
  	"# Parameter lambda = %f\n"
  	"# Simulation duration = %f\n\n",
  	N, (int) seed1,(int) seed2, (int) u_0_max, p_f_0, theta, beta, lambda, duration);
  
  char neuron_out[1024] = "#    Time  Type   Origin ";
  for (int i=0; i<10; i++) {
    char to_add[40];
    sprintf(to_add,"   MPP(%2d)    FP(%2d)",i,i);
    strcat(neuron_out,to_add);
  }
  char the_end[] = "... :\n";
  strcat(neuron_out,the_end);
  fprintf(fout_neuron,neuron_out);
  
  while (t_now < duration) {
    int event_type = 0;
    int n;
    int is_active; // var used to keep state of synapse of neuron that just spiked
    /* Get the time of next event*/
    double nu = beta*n_ceil_theta + lambda*f_sum;
    t_now += -log(unif())/nu; 
    /* Take care of a dead process */  
    if (n_ceil_theta == 0) {
      fprintf(fout_ntw,"# Activity gone.\n");
      fprintf(fout_neuron,"# Activity gone.\n");
      printf("The activity died at time: %10.6f\n",t_now);
      free(u);
      free(f);
      fclose(fout_neuron);
      fclose(fout_ntw);
      return 0;
    } 
    /* Attribute the event that was just generated by the network 
       to one of the neurons */			 
    if (beta * n_ceil_theta / nu > unif()) { // The event is a spike
      event_type = 1;
      n_total += 1;
      int v = (int) floor(unif()*n_ceil_theta);
      n = 0;
      int i = 0;
      for (; n<N; n++) { // attribute spike to neuron
        if (u[n] >= theta) {
          if (i == v) {
    	break;
          } else {
    	i++;
          }
        }
      }
      // n is now the index of the neuron that spiked
      u[n] = 0; // reset potential after spike
      n_ceil_theta -= 1; // decrease number of neurons above threshold
      if (f[n]) { // the neuron that spiked had an active synapse
        is_active = 1;
        for (size_t j=0; j < N; j++) {
          if (j != n) {
    	u[j] += 1; // increase the potential of the postsynaptic guys
    	if (u[j] >= theta && u[j]-theta < 1) {
    	  n_ceil_theta += 1; // If threshold was crossed increase n_ceil_theta
    	}
          }
        }
      } else { // the neuron that spiked had an inactive synapse
        is_active = 0;
        f[n] = true; // Make the synapse active
        f_sum += 1; // Increase the count of active synapses
      }
    } else { // A synapse gets inactivated
      event_type = 0;
      int v = (int) floor(unif()*f_sum);
      n = 0;
      int i = 0;
      for (; n<N; n++) { // choose the synapse that gets inactivated
        if (f[n]) {
          if (i == v) {
    	break;
          } else {
    	i++;
          }
        }
      }
      f[n] = false;
      f_sum -= 1;
    }
    /* Print the event's time, the network counting process and the 
       neuron of origin to 'out_ntw'. Print the event's time, the 
       membrane potential and the residual calcium of all 
       neurons as well as the mean membrane potential and mean residual 
       calcium to 'out_neuron'.*/
    if (event_type == 1) // make sure event was a spike and not a synapse inactivation 
        fprintf(fout_ntw,"%12.10f  %18d  %16d  %16d  %16d  %12d\n",
    	    t_now, n_total, n, n_ceil_theta, f_sum, is_active);
    
    fprintf(fout_neuron,"%12.10f  %2d  %2d ",
    	t_now,event_type,n);
    for (int i=0; i<N-1; i++)
      fprintf(fout_neuron,"%10d %10d ",(int) u[i], (int) f[i]);
    fprintf(fout_neuron,"%10d %10d\n",(int) u[N-1], (int) f[N-1]);  
  }

  free(u);
  free(f);
  fclose(fout_neuron);
  fclose(fout_ntw);  

  return 0;
}


/* main takes care of reading the input parameters and calls sun_sim */
int main(int argc, char *argv[]) {
  static char usage[] = \
    "usage %s -s --seed1=int -g --seed2=int [-n --network-size=int] ...\n"
    "         ... [-d --duration=double] [-l --lambda=double] ... \n"
    "         ... [-b --beta=double] [-t --theta=double] [-u --u_0=int] ...\n"
    "         ... [-f --f_0=double] [-o --out-name=string] ...\n\n"
    "  -s --seed1 <int>: first seed of the Xoroshiro128+ PRNG.\n"
    "  -g --seed2 <int>: second seed of the Xoroshiro128+ PRNG.\n"
    "  -n --network-size <int>: The number of neurons in the network (default 10).\n"
    "  -d --duration <double>: Simulation duration in sec (default 120).\n"
    "  -l --lambda <double>: Synaptic facilitation decay rate (default 50).\n"
    "  -b --beta <double>: Spike rate when potential exceeds threshold (default 5).\n"
    "  -t --theta <double>: Threshold to enter spiking state (default N/10).\n"
    "  -u --u_0 <int>: Initial max value of the membrane potential (default 10).\n"
    "                  Individual values are drawn uniformly between 0 and u_0.\n"
    "  -f --f_0 <double>: Initial prob. for a synapse to be facilitated (default 0.5).\n"
    "  -o --out-name <string>: Prefix of file names used to store results.\n"
    "                          A file called out_name_ntw is created with\n"
    "                          the spike time, the total number of spikes\n"
    "                          observed so far and the neuron of origin stored on\n"
    "                          three columns. Another file called out_name_neuron\n"
    "                          is created with the membrane potential and the\n"
    "                          synaptic state of each neuron.\n"
    "                          The default value of out_name is set to sim_res.\n\n" 
    "Simulates an homogenous network made of N neurons. Two variables are associated with each\n"
    "neuron:\n"
    "  a \"membrane potential\" u\n"
    "  a \"a synapse state\" f.\n"
    "A neuron spikes or its synapse gets inactive independently of each other with a rate:\n"
    "beta x u (if u >= theta) + lambda x f (if f == 1).\n"
    "The neuron that spiked WITH A FACILITATED SYNAPSE (f=1) increases the membrane potential\n"
    "of all the other neurons by an amount 1 and resets its own membrane potential to 0.\n"
    "The synaptic state of the neuron that just spiked becomes active if it was not already so.\n";
  char *filename;
  char output_ntw[512]="sim_res_ntw";
  char output_neuron[512]="sim_res_neuron";
  int out_set=0; 
  _Bool threshold_default=true;
  int N=10; // Network size
  double p_f_0=0.5; // Initial proba of active synapse
  unsigned int u_0_max=10; // Initial max membrane potential
  double beta=5;
  double theta = -1.0;
  double lambda=50.0;
  double duration=120.0;
  uint64_t seed1=20061001;
  uint64_t seed2=20110928;
  {int opt;
    static struct option long_options[] = {
      {"seed1",optional_argument,NULL,'s'},
      {"seed2",optional_argument,NULL,'g'},
      {"network-size",optional_argument,NULL,'n'},
      {"duration",optional_argument,NULL,'d'},
      {"lambda",optional_argument,NULL,'l'},
      {"beta",optional_argument,NULL,'b'},
      {"theta",optional_argument,NULL,'t'},
      {"u_0",optional_argument,NULL,'u'},
      {"f_0",optional_argument,NULL,'f'},
      {"out-name",optional_argument,NULL,'o'},
      {"help",no_argument,NULL,'h'},
      {NULL,0,NULL,0}
    };
    int long_index =0;
    while ((opt = getopt_long(argc,argv,
                              "hs:g:n:d:l:b:u:f:o:t:",
                              long_options,       
                              &long_index)) != -1) {
      switch(opt) {
      case 'o':
      {
        filename = optarg;
        out_set = 1;
      }
      break;
      case 's':
      {
        seed1 = (uint64_t) atoi(optarg);
      }
      break;
      case 'g':
      {
        seed2 = (uint64_t) atoi(optarg);
      }
      break;
      case 'n':
      {
        N = atoi(optarg);
        if (N <= 0) {
  	fprintf(stderr,"Network size should be > 0.\n");
  	return -1;
        }
      }
      break;
      case 'd':
      {
        duration = (double) atof(optarg);
        if (duration<=0.0) {
          fprintf(stderr,"duration should be > 0.\n");
          return -1;
        }      
      }
      break;
      case 'b':
      {
        beta = (double) atof(optarg);
        if (beta<=0.0) {
          fprintf(stderr,"β should be > 0.\n");
          return -1;
        }      
      }
      break;
      case 't':
      {
        theta = (double) atof(optarg);
        if (theta<=0.0) {
          fprintf(stderr,"θ should be > 0.\n");
          return -1;
        }      
        threshold_default=false;
      }
      break;
      case 'l':
      {
        lambda = (double) atof(optarg);
        if (lambda<0.0) {
          fprintf(stderr,"λ should be >= 0.\n");
          return -1;
        }      
      }
      break;
      case 'u':
      {
        u_0_max = (unsigned int) atoi(optarg);
        if (u_0_max <= 0.0) {
          fprintf(stderr,"Parameter u_0 should be > 0.\n");
          return -1;
        }      
      }
      break;
      case 'f':
      {
        p_f_0 = (double) atof(optarg);
        if (p_f_0 < 0.0 || p_f_0 > 1.0) {
          fprintf(stderr,"Parameter p_f_0 should be a probability.\n");
          return -1;
        }      
      }
      break;
      case 'h': printf(usage,argv[0]);
        return -1;
      default : fprintf(stderr,usage,argv[0]);
        return -1;
      }
    }
  }
  
  // Set output prefix name if not given
  if (out_set) {
    strcpy(output_ntw,filename);
    strcat(output_ntw,"_ntw");
    strcpy(output_neuron,filename);
    strcat(output_neuron,"_neuron");
  }
  if (threshold_default) {
    theta = N/10.0;
  }
  
  int res = run_sim(N, seed1, seed2, p_f_0, u_0_max, theta, beta,
		    lambda, duration,output_ntw,output_neuron);
  return res;
}
