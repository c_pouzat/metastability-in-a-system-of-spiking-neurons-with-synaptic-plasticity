! Modified from Arjen Markus' robust_newton.f90 by C. Pouzat --
!     
!  Example belonging to "Modern Fortran in Practice" by Arjen Markus
!
!  This work is licensed under the Creative Commons Attribution 3.0 Unported License.
!  To view a copy of this license, visit http://creativecommons.org/licenses/by/3.0/
!  or send a letter to:
!  Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041,
!    USA.
!
!  Robust version of the Newton-Raphson method
!
module robust_newton_m

  implicit none
  
  integer, parameter :: bracketed_root       = 1
  integer, parameter :: small_value_solution = 2
  integer, parameter :: convergence_reached  = 3
  integer, parameter :: invalid_start_value  = -1
  integer, parameter :: no_convergence       = -2
  integer, parameter :: invalid_arguments    = -3

contains

  subroutine find_root( f, f_prime, xinit, scalex, tolx, &
       smallf, maxevals, result, success, verbose )
  
    interface
       subroutine f( x, indomain, value )
         real, intent(in)      :: x
         logical, intent(out)  :: indomain
         real, intent(out)     :: value
       end subroutine f
    end interface
  
    interface
       function f_prime( x ) result(df)
         real, intent(in)   :: x
         real :: df
       end function f_prime
    end interface
  
    real, intent(in)     :: xinit
    real, intent(in)     :: scalex
    real, intent(in)     :: tolx
    real, intent(in)     :: smallf
    integer, intent(in)  :: maxevals
    real, intent(out)    :: result
    integer, intent(out) :: success
    logical, intent(in)  :: verbose
  
    real                 :: epsf
    real                 :: fx1
    real                 :: fx2
    real                 :: fxnew
    real                 :: fprime
    real                 :: x
    real                 :: xnew
    integer              :: i
    integer              :: evals
    logical              :: indomain
  
    result  = 0.0
    success = no_convergence
    
    !
    ! Sanity check
    !
    if ( scalex <= 0.0 .or. tolx <= 0.0 .or. smallf <= 0.0 ) then
       success = invalid_arguments
       return
    endif
      
    !
    ! Check the initial value
    !
    x = xinit
        
    call f( x, indomain, fx1 )
    evals = 1
    
    if ( .not. indomain ) then
       success = invalid_start_value
       return
    endif
    
      
    outerloop: do
  
       !
       ! Is the function value small enough?
       ! Then stop
       !
       if ( abs(fx1) <= smallf ) then
          success = small_value_solution
          result  = x
          exit
       endif
    
       !
       ! Determine the next estimate
       !
       fprime = f_prime(x)
       
       newxloop: do while ( evals < maxevals )
          xnew  = x - fx1 / fprime
          
          call f( xnew, indomain, fxnew )
          evals = evals + 1
          
          if ( .not. indomain ) then
             fx1  = fx1 / 2.0
          else
             exit newxloop
          endif
       end do newxloop
       
       fx1 = fxnew
       
       
       !
       ! Have we reached convergence?
       !
       if (verbose) write(*,*) 'Difference: ', abs(xnew-x), ' -- ', scalex * tolx
       if ( evals < maxevals ) then
          if ( abs(xnew-x) <= scalex * tolx ) then
             success = convergence_reached
             if ( abs(fx1) < smallf ) then
                success = small_value_solution
             endif
             result  = xnew
             if (verbose) write(*,*) 'Result: ',result
             exit outerloop
          endif
       else
          exit outerloop
       endif
         
  
       x = xnew
       if (verbose) write(*,*) evals, x
    enddo outerloop
  
    !
    ! Simply a small value or a bracketed root?
    !
    call f( x - scalex*tolx, indomain, fx1 )
    evals = evals + 1
    if ( indomain ) then
       call f( x + scalex*tolx, indomain, fx2 )
       evals = evals + 1
       if ( indomain ) then
          if ( fx1 * fx2 <= 0.0 ) then
             success = bracketed_root
          endif
       endif
    endif    
    if (verbose) write(*,*) 'Number of evaluations: ', evals
      
  end subroutine find_root

end module robust_newton_m


module qsd_target_m
  implicit none
  private
  integer :: n     ! network size
  integer :: theta ! threshold
  real :: eta      ! lambda / beta ratio

  public :: n, theta, eta, &
       qsd_target, &
       d_qsd_target, &
       qsd_dist

  contains
    
    subroutine qsd_target(x, indomain, value)
      implicit none
      real, intent(in)     :: x
      logical, intent(out) :: indomain
      real, intent(out)    :: value
      
      indomain = .true.
      if (x < 0) then
         indomain = .false.
         value = theta 
      else if (x > n) then
         indomain = .false.
         value = (n + theta)  - &
         n ** (theta + 1) / (n + eta) ** theta / (1.0 + eta)
      else
         value = (x + theta) - &
         n / (1.0 + eta) * (x  / (eta + x)) ** theta
      end if
    
    end subroutine qsd_target
    
    function d_qsd_target(x) result(dt)
      implicit none
      real, intent(in)     :: x
      real    :: dt
      
      dt = 1.0 - n * eta * theta / (eta + 1.0) *&
           (x / (eta + x)) ** (theta + 1) / x ** 2
    
    end function d_qsd_target
    

    subroutine qsd_dist(mu_theta_1, result)
      ! mu_theta_1 (real) is the estimate of what its name says
      ! result is a 2 x (theta+1) array
      !        whose rows are index from 0 to 1 and 
      !        whose columns are indexed from 0 to theta
      implicit none
      real, intent(in) ::    mu_theta_1
      real, allocatable, dimension(:,:), intent (in out) :: result
      real :: kappa
    
      result(1,theta) = mu_theta_1
      kappa = real(n) / (theta + mu_theta_1)
      result(0,theta) = (kappa - 1.0) * mu_theta_1
      block
        integer :: i
        real :: rho
        do i = 0,theta-1
           rho = (mu_theta_1 / (eta + mu_theta_1)) ** (i+1)
           result(1,i) = kappa * rho
           result(0,i) = (1.0 - rho) * kappa
        end do
      end block
    end subroutine qsd_dist

end module qsd_target_m    

program qsd

  use robust_newton_m, only : find_root
  use qsd_target_m, only : n, &   ! the number of neurons in the ntw
       theta, &                   ! the threshold
       eta, &                     ! lambda / beta (see below)
       qsd_target, &              ! subroutine computing mu_theta_1
       d_qsd_target, &            ! function returning the derivative of the above
       qsd_dist                   ! subroutine computing the QSD given mu_theta_1    

  implicit none

  real           :: beta          ! spiking rate at or above threshold
  real           :: lambda        ! un-facilitation rate 
  real           :: x             ! current value for mu_{theta,1}
  real           :: root          ! estimated value for mu_{theta,1}
  real           :: froot         ! target value at root
  integer        :: maxiter = 50  ! maximum number of target evaluations
  integer        :: conclusion    ! what was obtained
  logical        :: indomain      ! logical do we have 0 <= x <= theta
  logical        :: verbose       ! should newton algo. progress be printed?
  logical        :: both          ! should both roots be looked for?
  logical        :: full_qsd      ! should the full QSD be returned
  ! Array used to compute the output      
  real, allocatable, dimension(:,:) :: result

  call get_par_from_cli(n, beta, lambda, theta, verbose, both, full_qsd)
  eta = lambda/beta
  allocate(result(0:1,0:theta))
  result = 0.0
  
  x = real(n)
  if (verbose) write (*,*) "Starting from: ", x, " we get: "
  call find_root(qsd_target, d_qsd_target, x, 1.0, &
       1.0e-3, 1.0e-3, maxiter, root, conclusion, verbose)
  call qsd_target(root, indomain, froot)
  call qsd_dist(root, result)
  if (verbose) then
     write(*,*) root, froot, conclusion
     write(*,*) " "
  end if
  if (full_qsd) then
     if (verbose) write(*,*) "QSD: (mu_0_0, mu_0_1, mu_1_0, mu_1_1,...,"//&
          "mu_theta_0, mu_theta_1)"
     write(*,*) result
  else
     if (verbose) write(*,*) "mu_theta_1:"
     write(*,*) root
  end if
  

  if (both) then
     write (*,*) " "
     x = real(theta)
     if (verbose) write (*,*) "Starting from: ", x, " we get: "
     call find_root(qsd_target, d_qsd_target, x, 1.0, &
          1.0e-3, 1.0e-3, maxiter, root, conclusion, verbose)
     call qsd_target(root, indomain, froot)
     call qsd_dist(root, result)
     if (verbose) then
        write(*,*) root, froot, conclusion
        write(*,*) " "
     end if
     if (full_qsd) then
        if (verbose) write(*,*) "QSD: (mu_0_0, mu_0_1, mu_1_0, mu_1_1,...,"//&
             "mu_theta_0, mu_theta_1)"
        write(*,*) result
     else
        if (verbose) write(*,*) "mu_theta_1:"
        write(*,*) root
     end if
  end if
  
contains

    subroutine get_par_from_cli(n, beta, lambda, theta, verbose, both, full_qsd)
      integer, intent(out) :: n
      real, intent(out)    :: beta
      real, intent(out)    :: lambda
      integer, intent(out) :: theta
      logical, intent(out) :: verbose
      logical, intent(out) :: both
      logical, intent(out) :: full_qsd
      character(len=10)    :: arg
    
      call get_command_argument(1, arg)
      read (arg, *) n
      if (n <= 0) stop &
          "The number of neurons should be > 0."    
      call get_command_argument(2, arg)
      read (arg, *) beta
      if (beta <= 0) &
          stop "The spiking rate should be > 0."
      call get_command_argument(3, arg)
      read (arg, *) lambda
      if (lambda <= 0) &
          stop "The un-facilitation rate should be > 0."
      call get_command_argument(4, arg)
      read (arg, *) theta
      if ((theta <= 0) .or. (theta >= n)) stop &
          "Expect 0 < theta < number of neurons."    
      call get_command_argument(5, arg)
      read (arg, *) verbose
      call get_command_argument(6, arg)
      read (arg, *) both
      call get_command_argument(7, arg)
      read (arg, *) full_qsd
    end subroutine get_par_from_cli 
  
end program qsd
