program ntw_ecdf
  ! Declare what to use in what module    
  use iso_fortran_env, only: int32, real64, &
        stdin => input_unit, &
        stdout => output_unit, &
        stderr => error_unit    
  implicit none
  ! Declare the program variables and parameters    
  integer(kind=int32) :: facilitation_state ! Specified in the program call
  integer(kind=int32) :: membrane_potential ! Specified in the program call     
  integer(kind=int32) :: n_neuron ! Number of neurons in the network
  integer(kind=int32) :: theta ! Membrane potential threshold
  integer(kind=int32) :: n_rep ! Number of replicates   
  real(kind=real64) :: period ! Sampling period
  real(kind=real64) :: duration ! Simulation duration
  character(len=1000) :: dummy1 ! For individual input lines storage
  integer :: i, j, rep_idx, pop_idx ! indices
  integer(kind=int32) :: n_period ! number of sampling periods in a complete sim.
  ! Array containing the network state at a given time      
  integer(kind=int32), allocatable, dimension(:,:) :: state_pop
  ! Array used to compute the occupation of a given (u,f) accross replicates      
  integer(kind=int32), allocatable, dimension(:,:) :: mp
  ! Array used to compute the output      
  real(kind=real64), allocatable, dimension(:,:) :: result
  ! Define strings introducing specific parts of the output
  ! Prefix of network size specification      
  character(len=*), parameter :: pre_n_neuron = "# Simulation of a networks with "
  ! Prefix of theta specification            
  character(len=*), parameter :: pre_theta = "# Parameter theta = "
  ! Prefix of number of replicates specification      
  character(len=*), parameter :: pre_n_rep = "# Number of replicates = "
  ! Prefix of simulation duration specification      
  character(len=*), parameter :: pre_duration = "# Simulation duration = "
  ! Prefix of sampling period specification      
  character(len=*), parameter :: pre_period = "# Sampling period = "
  ! Prefix identifying the beginning or the end of a replicate      
  character(len=*), parameter :: pre_rep = "# Replicate "
  ! Suffix identifying the end of a replicate            
  character(len=*), parameter :: end_rep = " done."
  ! Prefix identifying the beginning of the "statistics" part of the input      
  character(len=*), parameter :: pre_stats = "# Summary statistics: "
  ! Prefix identifying the replicate death      
  character(len=*), parameter :: pre_death = "# Dead network at time: "           
  ! Read target (u,f) from the command line    
  call get_par_from_cli(facilitation_state, membrane_potential)
  ! Get n_neuron, theta, n_rep, period and duration from input    
  read (stdin,'(a)') dummy1
  read (dummy1(len(pre_n_neuron)+1:len(pre_n_neuron)+7),'(i6)') n_neuron
  do i=1,6 ! Read next six lines     
    read (stdin,'(a)') dummy1
  end do      
  read (dummy1(len(pre_theta)+1:len(pre_theta)+7),'(i6)') theta
  if (membrane_potential > theta) &
        stop "Membrane potential should be <= theta."    
  read (stdin,'(a)') dummy1        
  read (dummy1(len(pre_n_rep)+1:len(pre_n_rep)+7),'(i6)') n_rep
  read (stdin,'(a)') dummy1    
  read (dummy1(len(pre_duration)+1:len(pre_duration)+7),'(f6.0)') duration
  read (stdin,'(a)') dummy1    
  read (dummy1(len(pre_period)+1:len(pre_period)+7),'(f6.2)') period
  read (stdin,'(a)') dummy1        
  ! Initializes n_period and allocates state_pop and mp
  n_period = ceiling(duration/period,int32)
  allocate(state_pop(0:1,0:theta))
  allocate(mp(0:n_period,0:n_neuron))
  mp = 0  
  ! Read (f,u) state from input
  read (stdin,'(a)') dummy1
  ! As long as the "statistics" part of the input is not reached      
  replicate_loop: do while (index(dummy1,pre_stats) == 0)
    replicate_end: if (index(dummy1,pre_rep) > 0) then
      ! A replicate starts or stops
      ! Get the replicate index  
      read(dummy1(len(pre_rep)+1:len(pre_rep)+7),'(i6)') rep_idx
      ! Check that we are not at the replicate's end  
      replicate_start: if (index(dummy1,end_rep) .eq. 0) then
        period_loop: do i = 0,n_period ! Read the successive states of the replicate
          read (stdin,'(a)') dummy1
          if (index(dummy1,pre_death) > 0) exit ! If rep. dead exit loop
          read (dummy1(10:),'(*(i4))') state_pop
          ! Get the number of neurons in state (f,u)
          pop_idx = state_pop(facilitation_state, membrane_potential)
          ! Increase the corresponding entry in mp
          mp(i,pop_idx) = mp(i,pop_idx) + 1
        end do period_loop
      end if replicate_start  
    end if replicate_end
    read (stdin,'(a)') dummy1    
  end do replicate_loop    
  ! Compute and write results
  period_loop2: do i = 0,n_period
    neuron_loop: do j = 1,n_neuron
      mp(i,j) = mp(i,j-1) + mp(i,j)   
    end do neuron_loop  
  end do period_loop2   
        
  allocate(result(0:n_period,0:n_neuron))
  result = 0
  period_loop3: do i = 0, n_period
    result(i,:) = real(mp(i,:),kind=real64)/&
        real(mp(i,n_neuron),kind=real64)    
  end do period_loop3
  
  write (*,'(a,i6,a)') "# There are ", n_period+2, " columns."      
  write (*,'(a,i6,a)') "# There are ", n_neuron+1, " rows."
  write (*,'(a)') " "      
  neuron_loop3: do j = 0, n_neuron    
    write (*, '(i6)', advance='no') j  
    write (*, '(*(f7.3,2x))') result(:,j)    
  end do neuron_loop3   
  
  contains
  subroutine get_par_from_cli(facilitation_state, membrane_potential)
    integer(kind=int32), intent(out) :: facilitation_state
    integer(kind=int32), intent(out) :: membrane_potential
    character(len=10) :: arg
  
    call get_command_argument(1, arg)
    read (arg, *) membrane_potential
    if (membrane_potential < 0) stop &
        "The membrane potential should be >= 0."    
    call get_command_argument(2, arg)
    read (arg, *) facilitation_state
    if ((facilitation_state < 0) .or. (facilitation_state > 1)) &
        stop "The facilitation state should be 0 or 1."              
  end subroutine get_par_from_cli 
      
end program ntw_ecdf
