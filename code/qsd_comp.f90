! Modified from Arjen Markus' robust_newton.f90 by C. Pouzat --
!     
!  Example belonging to "Modern Fortran in Practice" by Arjen Markus
!
!  This work is licensed under the Creative Commons Attribution 3.0 Unported License.
!  To view a copy of this license, visit http://creativecommons.org/licenses/by/3.0/
!  or send a letter to:
!  Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041,
!    USA.
!
!  Robust version of the Newton-Raphson method
!
module robust_newton_m

  implicit none
  
  integer, parameter :: bracketed_root       = 1
  integer, parameter :: small_value_solution = 2
  integer, parameter :: convergence_reached  = 3
  integer, parameter :: invalid_start_value  = -1
  integer, parameter :: no_convergence       = -2
  integer, parameter :: invalid_arguments    = -3

contains

  subroutine find_root( f, f_prime, xinit, scalex, tolx, &
       smallf, maxevals, result, success, verbose )
  
    interface
       subroutine f( x, indomain, value )
         real, intent(in)      :: x
         logical, intent(out)  :: indomain
         real, intent(out)     :: value
       end subroutine f
    end interface
  
    interface
       function f_prime( x ) result(df)
         real, intent(in)   :: x
         real :: df
       end function f_prime
    end interface
  
    real, intent(in)     :: xinit
    real, intent(in)     :: scalex
    real, intent(in)     :: tolx
    real, intent(in)     :: smallf
    integer, intent(in)  :: maxevals
    real, intent(out)    :: result
    integer, intent(out) :: success
    logical, intent(in)  :: verbose
  
    real                 :: epsf
    real                 :: fx1
    real                 :: fx2
    real                 :: fxnew
    real                 :: fprime
    real                 :: x
    real                 :: xnew
    integer              :: i
    integer              :: evals
    logical              :: indomain
  
    result  = 0.0
    success = no_convergence
    
    !
    ! Sanity check
    !
    if ( scalex <= 0.0 .or. tolx <= 0.0 .or. smallf <= 0.0 ) then
       success = invalid_arguments
       return
    endif
      
    !
    ! Check the initial value
    !
    x = xinit
        
    call f( x, indomain, fx1 )
    evals = 1
    
    if ( .not. indomain ) then
       success = invalid_start_value
       return
    endif
    
      
    outerloop: do
  
       !
       ! Is the function value small enough?
       ! Then stop
       !
       if ( abs(fx1) <= smallf ) then
          success = small_value_solution
          result  = x
          exit
       endif
    
       !
       ! Determine the next estimate
       !
       fprime = f_prime(x)
       
       newxloop: do while ( evals < maxevals )
          xnew  = x - fx1 / fprime
          
          call f( xnew, indomain, fxnew )
          evals = evals + 1
          
          if ( .not. indomain ) then
             fx1  = fx1 / 2.0
          else
             exit newxloop
          endif
       end do newxloop
       
       fx1 = fxnew
       
       
       !
       ! Have we reached convergence?
       !
       if (verbose) write(*,*) 'Difference: ', abs(xnew-x), ' -- ', scalex * tolx
       if ( evals < maxevals ) then
          if ( abs(xnew-x) <= scalex * tolx ) then
             success = convergence_reached
             if ( abs(fx1) < smallf ) then
                success = small_value_solution
             endif
             result  = xnew
             if (verbose) write(*,*) 'Result: ',result
             exit outerloop
          endif
       else
          exit outerloop
       endif
         
  
       x = xnew
       if (verbose) write(*,*) evals, x
    enddo outerloop
  
    !
    ! Simply a small value or a bracketed root?
    !
    call f( x - scalex*tolx, indomain, fx1 )
    evals = evals + 1
    if ( indomain ) then
       call f( x + scalex*tolx, indomain, fx2 )
       evals = evals + 1
       if ( indomain ) then
          if ( fx1 * fx2 <= 0.0 ) then
             success = bracketed_root
          endif
       endif
    endif    
    if (verbose) write(*,*) 'Number of evaluations: ', evals
      
  end subroutine find_root

end module robust_newton_m


module qsd_target_m
  implicit none
  private
  integer :: n     ! network size
  integer :: theta ! threshold
  real :: eta      ! lambda / beta ratio

  public :: n, theta, eta, &
       qsd_target, &
       d_qsd_target, &
       qsd_dist

  contains
    
    subroutine qsd_target(x, indomain, value)
      implicit none
      real, intent(in)     :: x
      logical, intent(out) :: indomain
      real, intent(out)    :: value
      
      indomain = .true.
      if (x < 0) then
         indomain = .false.
         value = theta - n / (1.0 + eta)
      else if (x > n) then
         indomain = .false.
         value = (n + theta)  - &
         n ** (theta + 1) / (n + eta) ** theta
      else
         value = (x + theta) - &
         n / (1.0 + eta) * (x  / (eta + x)) ** theta
      end if
    
    end subroutine qsd_target
    
    function d_qsd_target(x) result(dt)
      implicit none
      real, intent(in)     :: x
      real    :: dt
      
      dt = 1.0 - n * eta * theta / (eta + 1.0) *&
           (x / (eta + x)) ** (theta + 1) / x ** 2
    
    end function d_qsd_target
    

    subroutine qsd_dist(mu_theta_1, result)
      ! mu_theta_1 (real) is the estimate of what its name says
      ! result is a 2 x (theta+1) array
      !        whose rows are index from 0 to 1 and 
      !        whose columns are indexed from 0 to theta
      implicit none
      real, intent(in) ::    mu_theta_1
      real, allocatable, dimension(:,:), intent (in out) :: result
      real :: kappa
    
      result(1,theta) = mu_theta_1
      kappa = real(n) / (theta + mu_theta_1)
      result(0,theta) = (kappa - 1.0) * mu_theta_1
      block
        integer :: i
        real :: rho
        do i = 0,theta-1
           rho = (mu_theta_1 / (eta + mu_theta_1)) ** (i+1)
           result(1,i) = kappa * rho
           result(0,i) = (1.0 - rho) * kappa
        end do
      end block
    end subroutine qsd_dist

end module qsd_target_m    

program qsd_comp
  ! Re-use former blocks
  use iso_fortran_env, only: int32, real64, &
        stdin => input_unit, &
        stdout => output_unit, &
        stderr => error_unit
  
  use robust_newton_m, only : find_root
  use qsd_target_m, only : n, &   ! the number of neurons in the ntw
       theta, &                   ! the threshold
       eta, &                     ! lambda / beta (see below)
       qsd_target, &              ! subroutine computing mu_theta_1
       d_qsd_target, &            ! function returning the derivative of the above
       qsd_dist                   ! subroutine computing the QSD given mu_theta_1    
  implicit none
  integer(kind=int32) :: n_rep ! Number of replicates   
  real(kind=real64) :: period ! Sampling period
  real(kind=real64) :: duration ! Simulation duration
  character(len=5000) :: dummy1 ! For individual input lines storage
  ! Array used to compute the output      
  real(kind=real64), allocatable, dimension(:,:) :: result
  ! Define strings introducing specific parts of the output
  ! Prefix of network size specification      
  character(len=*), parameter :: pre_n_neuron = "# Simulation of a networks with "
  ! Prefix of theta specification            
  character(len=*), parameter :: pre_theta = "# Parameter theta = "
  ! Prefix of number of replicates specification      
  character(len=*), parameter :: pre_n_rep = "# Number of replicates = "
  ! Prefix of simulation duration specification      
  character(len=*), parameter :: pre_duration = "# Simulation duration = "
  ! Prefix of sampling period specification      
  character(len=*), parameter :: pre_period = "# Sampling period = "
  ! Prefix identifying the beginning or the end of a replicate      
  character(len=*), parameter :: pre_rep = "# Replicate "
  ! Suffix identifying the end of a replicate            
  character(len=*), parameter :: end_rep = " done."
  ! Prefix identifying the beginning of the "statistics" part of the input      
  character(len=*), parameter :: pre_stats = "# Summary statistics: "
  ! Prefix identifying the replicate death      
  character(len=*), parameter :: pre_death = "# Dead network at time: " 
  real           :: beta          ! spiking rate at or above threshold
  real           :: lambda        ! un-facilitation rate 
  real           :: x             ! current value for mu_{theta,1}
  real           :: root          ! estimated value for mu_{theta,1}
  real           :: froot         ! target value at root
  integer        :: maxiter = 30  ! maximum number of target evaluations
  integer        :: conclusion    ! what was obtained
  logical        :: indomain      ! logical do we have 0 <= x <= theta
  ! should newton algo. progress be printed?
  logical, parameter :: verbose = .false.
  ! should both roots be looked for?
  logical, parameter :: both = .false. 
  ! Array used to compute the theoretical QSD      
  real, allocatable, dimension(:,:) :: qsd_theo

  read (stdin,'(a)') dummy1
  read (dummy1(len(pre_n_neuron)+1:len(pre_n_neuron)+7),'(i6)') n
  block
    integer :: i
    do i=1,4 ! Read next four lines     
       read (stdin,'(a)') dummy1
    end do
  end block
  read (dummy1(len(pre_theta)+1:len(pre_theta)+7),'(f5.1)') beta
  read (stdin,'(a)') dummy1
  read (dummy1(len(pre_theta)+1:len(pre_theta)+7),'(f5.1)') lambda
  read (stdin,'(a)') dummy1
  read (dummy1(len(pre_theta)+1:len(pre_theta)+7),'(i6)') theta
  read (stdin,'(a)') dummy1        
  read (dummy1(len(pre_n_rep)+1:len(pre_n_rep)+7),'(i6)') n_rep
  read (stdin,'(a)') dummy1    
  read (dummy1(len(pre_duration)+1:len(pre_duration)+7),'(f6.0)') duration
  read (stdin,'(a)') dummy1    
  read (dummy1(len(pre_period)+1:len(pre_period)+7),'(f6.2)') period
  read (stdin,'(a)') dummy1
  
  write (stdout,'(a, i5)')   "# N            = ", n
  write (stdout,'(a, f5.1)') "# beta         = ", beta
  write (stdout,'(a, f5.1)') "# lambda       = ", lambda
  write (stdout,'(a, i5)')   "# theta        = ", theta
  write (stdout,'(a, i6)')   "# N replicates = ", n_rep
  write (stdout,'(a, f6.0)') "# duration     = ", duration
  write (stdout,'(a, f6.2)') "# period       = ", period
  write (stdout,*) " "  

  eta = lambda/beta
  allocate(qsd_theo(0:1,0:theta))
  qsd_theo = 0.0
  
  x = real(n)
  call find_root(qsd_target, d_qsd_target, x, 1.0, &
       1.0e-5, 1.0e-5, maxiter, root, conclusion, verbose)
  call qsd_target(root, indomain, froot)
  call qsd_dist(root, qsd_theo)

  fast_forward_loop: do while (index(dummy1,pre_stats) == 0)
     read (stdin,'(a)') dummy1
  end do fast_forward_loop

  read (stdin,'(a)') dummy1 ! skip initial state
  write (stdout,'(a)') "# Time       Nb    chi2   max rel. diff.:"
  at_period: block
    integer :: n_period
    integer :: period_idx
    real :: chi2, rel_diff
    real, allocatable, dimension(:) :: summary_data
    real, allocatable, dimension(:,:) :: mu, sigma
    allocate(summary_data(1:4*(theta+1)+2))
    allocate(mu(0:1,0:theta))
    allocate(sigma(0:1,0:theta))
    n_period = ceiling(duration/period)
    period_loop: do period_idx = 1, n_period
       read (stdin,'(a)') dummy1
       read (dummy1,*) summary_data
       mu = reshape(summary_data(3:2+2*(theta+1)),shape(mu))
       sigma = reshape(summary_data(3+2*(theta+1):),shape(sigma))
       chi2 = sum(((mu - qsd_theo)/sigma)**2)
       rel_diff = maxval(abs(mu - qsd_theo)/qsd_theo)
       write (stdout,'(f6.2, tr3,i6, tr3, f5.1, tr3, f4.3)') &
            summary_data(1), int(summary_data(2)), chi2, rel_diff
    end do period_loop
  end block at_period
  
end program qsd_comp
