set terminal pngcairo size 640,480
set output "figs/sec5_n500t100b10l5_fig_1.png"
set title 'Number of replicates still alive (β=10, λ=5)'
set grid
set key at 14, 30000
set xlabel 'Time'
set ylabel 'Number of replicates'
theo(x) = 100000*exp(-1)
set format x "%g"
set logscale y
plot [0.05:15] [1000:100000] 'simulations/sec5_n500t100b10l5' \
     index 100000 u 1:2 with lines lc 'orange' lw 2 title 'N=500, θ=100', \
     'simulations/sec5_n50t10b10l5' index 100000 u 1:2 with lines lc 'blue' lw 2 title 'N=50, θ=10', \
     'simulations/sec5_n5t1b10l5' index 100000 u 1:2 with lines lc 'black' lw 2 title 'N=5, θ=1',\
     theo(x) with lines dashtype 0 lc 'black' lw 2 title ''
unset logscale y
