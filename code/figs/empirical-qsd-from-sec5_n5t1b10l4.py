def empirical_qsd(sim_fname,
                  theo_fname):
    """Read simulation output stored in 'sim_fname' and analytical QSD
    properties contained in theo_fname to produce the empirical QSD.

    Parameters
    ----------
    sim_fname: name of a file containing a simulation output.
    theo_fname: name of the file containing the computed QSD.
    
    Returns
    --------
    A tuple made of:
    - a list with the tuple representation of the R* states
    - a list with the theoretical QSD
    - a list of lists, one sub-list per time step, each sub-list contains
      the number of replicates in each state making R*
    - a list with as many element as time steps, each element contains the
      number of replicates still alive at that time
    - a string to be used as a header when writing the results to a file.
    """
    from qsd import map2conf
    R_star_tpl_lst = []
    R_star_prob_lst = []
    with open(theo_fname, 'r') as theo:
        line = theo.readline()
        if not "Perron-Frobenius eigenvalue and eigenvector" in line:
            raise ValueError("Wrong analytical QSD file.")
        for i in range(4):
            line = theo.readline()
        line = theo.readline()
        while "Prob." in line:
           tpl = map2conf(int(((line.split(')'))[0]).split('(')[1]))
           R_star_tpl_lst.append(tpl)
           R_star_prob_lst.append(float(((line.split(':'))[1])))
           line = theo.readline()
    print("R* contains " + str(len(R_star_tpl_lst)) + " states.")
    pre_n_neuron = "# Simulation of a networks with "
    pre_beta = "# Parameter beta = "
    pre_lambda = "# Parameter lambda = "
    pre_theta = "# Parameter theta = "
    pre_n_rep = "# Number of replicates = "
    pre_duration = "# Simulation duration = "
    pre_period = "# Sampling period = "
    pre_rep = "# Replicate"
    end_rep = "done.\n"
    pre_death = "# Dead"
    pre_stats = "# Summary statistics: "
    with open(sim_fname,'r') as f:
        line = f.readline()
        line = line.removeprefix(pre_n_neuron)
        nb_neurons = int(line.split()[0])
        line = f.readline()
        line = f.readline()
        line = f.readline()
        line = f.readline()
        line = line.removeprefix(pre_beta)
        beta = float(line.split()[0])
        line = f.readline()
        line = line.removeprefix(pre_lambda)
        lmbda = float(line.split()[0])
        line = f.readline()
        line = line.removeprefix(pre_theta)
        theta = int(line.split()[0])
        line = f.readline()
        line = line.removeprefix(pre_n_rep)
        nrep = int(line.split()[0])
        line = f.readline()
        line = line.removeprefix(pre_duration)
        duration = float(line.split()[0])
        line = f.readline()
        line = line.removeprefix(pre_period)
        period = float(line.split()[0])
        n_steps = int(duration / period + 1)
        res = [[0]*len(R_star_tpl_lst) for i in range(n_steps)]
        still_alive = [0]*n_steps
        rep_idx = 0
        line = f.readline()
        line2 = line.removeprefix(pre_stats)
        line3 = line.removeprefix(pre_rep)
        while len(line) == len(line2): # loop on replicates
            if len(line3) < len(line): # a replicate starts or stops
                rep_idx = int(line3.split()[0])
                line4 = line.removesuffix(end_rep)
                if len(line4) == len(line): # not at replicate end
                    for i in range(n_steps):
                        line = f.readline()
                        line_d = line.removeprefix(pre_death)
                        if len(line_d) < len(line):
                            break
                        still_alive[i] += 1
                        elt = line.split()
                        if len(elt) >= 5:
                            tpl = (int(elt[4]),int(elt[3]), int(elt[2]),int(elt[1]))
                            if tpl in R_star_tpl_lst:
                                idx = R_star_tpl_lst.index(tpl)
                                res[i][idx] += 1
                line = f.readline()
                line2 = line.removeprefix(pre_stats)
                line3 = line.removeprefix(pre_rep)
            else:
                line = f.readline()
                line2 = line.removeprefix(pre_stats)
                line3 = line.removeprefix(pre_rep)
        header = "# Simulation output stored in: " + sim_fname + "\n"
        header += "# QSD stored in: " + theo_fname + "\n"
        header += pre_n_neuron + str(nb_neurons) + "\n"
        header += pre_beta + str(beta) + "\n"
        header += pre_lambda + str(lmbda) + "\n"
        header += pre_theta + str(theta) + "\n"
        header += pre_n_rep + str(nrep) + "\n"
        header += pre_duration + str(duration) + "\n"
        header += pre_period + str(period) + "\n"
        header += "\n"
        return R_star_tpl_lst, R_star_prob_lst, res, still_alive, header

tpl, prob, qsd_emp, pop, head = empirical_qsd('simulations/sec5_n5t1b10l4',
                                              'explicit/quasi_stationary_N5b10l4.r')

def write_qsd(R_star_tpl_lst,
              R_star_prob_lst,
              res,
              still_alive,
              header,
              out_fname,
              times):
    """Write to file named 'out_fname' the content of the first five
    parameters at the times specified by 'times'

    Parameters
    ----------
    R_star_tpl_lst: list with the tuple representation of the states of R*
    R_star_prob_lst: list with the theoretical QSD
    res: list of sub-lists containing the estimated QSD at each sampled time
    still_alive: list with the number of repicates still alive at each time
    header: a string with the header of the output file
    out_fname: name of the file where the results are written
    times: a set of times at which to write the results

    Returns
    -------
    Nothing, the function is used for its side effect, a file is written.
    """
    from qsd import map2int
    period = float(header.split("# Sampling period = ")[1])
    keep_idx = [round(t/period) for t in times]
    sub_res = [res[idx] for idx in keep_idx]
    sub_alive = [still_alive[idx]  for idx in keep_idx]
    with open(out_fname,'w') as f:
        f.write(header)
        f.write("# Headcounts at each time for each state in R*\n")
        f.write("#  The first column contains the representation of the state.\n")
        f.write("#  The following columns contain the sates headcounts at the different times.\n")
        line = "# state " + \
            " ".join([" {"+str(i)+":>6.1f}" for
                      i in range(len(times))]) + \
                      "\n"
        f.write(line.format(*times))
        content = ["{"+str(i)+":>7d}" for i in range(len(times)+1)]
        line = " ".join(content) + "\n"
        for s_idx in range(len(R_star_prob_lst)):
            lst = [map2int(R_star_tpl_lst[s_idx])]
            lst += [sub_res[t_idx][s_idx] for t_idx in range(len(times))]
            f.write(line.format(*lst))
        f.write("\n")
        f.write("\n")

        f.write("# Normalized Headcounts at each time for each state in R*\n")
        f.write("#  The first column contains the representation of the state.\n")
        f.write("#  The next column contains the theoretical QSD.\n")
        f.write("#  The following columns contain the estimated QSD at the different times.\n")
        f.write("#  The following columns contain:\n")
        line = "# state     QSD " + \
            " ".join(["{"+str(i)+":>7.1f}" for
                      i in range(len(times))]) + \
                      "\n"
        f.write(line.format(*times))
        normed_res = [[sub_res[t_idx][s_idx]/sub_alive[t_idx] for
                       s_idx in range(len(R_star_prob_lst))] for
                       t_idx in range(len(times))]
        content = ["{0:>7d}","{1:>7.3f}"]
        content += ["{"+str(i)+":>7.3f}" for i in range(2,len(times)+2)]
        line = " ".join(content) + "\n"
        for s_idx in range(len(R_star_prob_lst)):
            lst = [map2int(R_star_tpl_lst[s_idx])]
            lst += [R_star_prob_lst[s_idx]]
            lst += [normed_res[t_idx][s_idx] for t_idx in range(len(times))]
            f.write(line.format(*lst))
 

write_qsd(tpl, prob, qsd_emp, pop, head,"simulations/sec5_n5t1b10l4_stats",[0.5,1,1.5,2])
