set terminal pngcairo size 640,480
set output "figs/sec5_n5t1b10l4_fig_2.png"
theo_s(x,slope,offset) = 100000*exp(-slope*(x-offset))
theo_o(x, val) = val
set multiplot title "N=5, θ=1, β=10, λ=4" layout 1,2  spacing 0.05,0.05
set grid
set title 'Number of replicates still alive'
set grid
unset key
set xlabel 'Time'
set ylabel 'Number of replicates'
set logscale y
plot [0:4] [1000:100000] 'simulations/sec5_n5t1b10l4' \
     index 100000 u 1:2 with lines lc 'black' lw 3, \
     theo_s(x,1.52,0.16) w lines lc 'orange' lw 2
unset logscale y
set title 'Mean headcounts'
set xlabel 'Time'
set ylabel 'Mean nb of neurons'
set bars small
set key at 0.4,4.7 top left spacing 2
plot [0:4] [0:5] 'simulations/sec5_n5t1b10l4' index 100000 u 1:6 \
     w lines pt 2 lc 'black' lw 2 title '(u,f)=(θ,1)',\
     'simulations/sec5_n5t1b10l4' index 100000 u 1:5 \
     w lines pt 2 lc 'red' lw 2 title '(u,f)=(θ,0)',\
     'simulations/sec5_n5t1b10l4' index 100000 u 1:4 \
     w lines pt 2 lc 'blue' lw 2 title '(u,f)=(0,1)',\
     'simulations/sec5_n5t1b10l4' index 100000 u 1:3 \
     w lines pt 2 lc 'orange' lw 2 title '(u,f)=(0,0)',\
     theo_o(x,2.125) w lines lc 'black' lt 2 lw 2 title '',\
     theo_o(x,1.135) w lines lc 'red' lt 2 lw 2 title '',\
     theo_o(x,1.398) w lines lc 'blue' lt 2 lw 2 title '',\
     theo_o(x,0.342) w lines lc 'orange' lt 2 lw 2 title ''
     
unset multiplot
