set terminal pngcairo size 640,480
set output "figs/emp-and-theo-qsd-from-sec5_n5t1b10l4_gp.png"
set multiplot layout 2,1  spacing 0.05,0.05
set grid
set xtics 1
set title 'Empirical R* population distributions'
set xlabel ''
set ylabel 'Frequency'
set format x ''
set key at 10, 10000
plot [0:30] [0:12000] 'simulations/sec5_n5t1b10l4_stats' \
     index 0 u ($0+1):2 with lp lc 'orange' lw 2 title 't=0.5', \
     '' index 0 u ($0+1):3 with lp lc 'blue' lw 2 title 't=1.0', \
     '' index 0 u ($0+1):4 with lp lc 'black' lw 2 title 't=1.5', \
     '' index 0 u ($0+1):5 with lp lc 'gray' lw 2 title 't=2.0'
set title 'Empirical and theoretical QSD'
set grid
unset key 
set xlabel 'State index'
set ylabel 'Probability'
plot [0:30] [0:0.2] 'simulations/sec5_n5t1b10l4_stats' \
     index 1 u ($0+1):3 with l lc 'red' lw 4 title 'theoretical', \
     '' index 1 u ($0+1):3 with l lc 'orange' lw 2 title 't=0.5', \
     '' index 1 u ($0+1):4 with l lc 'blue' lw 2 title 't=1.0', \
     '' index 1 u ($0+1):5 with l lc 'black' lw 2 title 't=1.5', \
     '' index 1 u ($0+1):6 with l lc 'gray' lw 2 title 't=2.0'
unset multiplot
