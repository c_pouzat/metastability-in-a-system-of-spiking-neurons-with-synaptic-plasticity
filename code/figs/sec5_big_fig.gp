set terminal pngcairo size 640,700
set output "figs/sec5_big_fig.png"
theo_s(x,slope,offset) = 100000*exp(-slope*(x-offset))
theo_o(x, val) = val
set multiplot title "N=5, θ=1, β=10" layout 3,2  spacing 0.05,0.05
set grid
unset key
set xtics 0.5 format ""
set logscale y
set title 'Nb rep. alive'
set ylabel ''
set xlabel ''
plot [0:2] [1000:100000] 'simulations/sec5_n5t1b10l3' \
     index 100000 u 1:2 with lines lc 'black' lw 3, \
     theo_s(x,0.776,0.16) w lines lc 'orange' lw 2
unset logscale y
set title 'Mean headcounts'
set ylabel ''
set xlabel ''
plot [0:2] [0:5] 'simulations/sec5_n5t1b10l3' index 100000 u 1:6\
     w lines lc 'black' lw 2,\
     theo_o(x,2.411) w lines lc 'black' lt 2,\
     'simulations/sec5_n5t1b10l3' index 100000 u 1:5 \
     w lines lc 'red' lw 2,\
     theo_o(x,0.985) w lines lc 'red',\
     'simulations/sec5_n5t1b10l3' index 100000 u 1:4 \
     w lines lc 'blue' lw 2,\
     theo_o(x,1.371) w lines lc 'blue' lt 2,\
     'simulations/sec5_n5t1b10l3' index 100000 u 1:3 \
     w lines lc 'orange' lw 2,\
     theo_o(x,0.234) w lines lc 'orange' lt 2
set logscale y
set title ''
set ylabel ''
set xlabel ''
plot [0:2] [1000:100000] 'simulations/sec5_n5t1b10l5' \
     index 100000 u 1:2 with lines lc 'black' lw 3, \
     theo_s(x,2.442,0.16) w lines lc 'orange' lw 2
unset logscale y
set title ''
set ylabel ''
set xlabel ''
plot [0:2] [0:5] 'simulations/sec5_n5t1b10l5' index 100000 u 1:6:(2*$10) \
     w lines lc 'black' lw 2,\
     theo_o(x,1.916) w lines lc 'black' lt 2,\
     'simulations/sec5_n5t1b10l5' index 100000 u 1:5 \
     w lines lc 'red' lw 2,\
     theo_o(x,1.24) w lines lc 'red',\
     'simulations/sec5_n5t1b10l5' index 100000 u 1:4 \
     w lines lc 'blue' lw 2,\
     theo_o(x,1.398) w lines lc 'blue' lt 2,\
     'simulations/sec5_n5t1b10l5' index 100000 u 1:3 \
     w lines lc 'orange' lw 2,\
     theo_o(x,0.446) w lines lc 'orange' lt 2
set format x "%g"
set logscale y
set title ''
set ylabel ''
set xlabel 'Time'
plot [0:2] [1000:100000] 'simulations/sec5_n5t1b10l8' \
     index 100000 u 1:2 with lines lc 'black' lw 3, \
     theo_s(x,5.889,0.12) w lines lc 'orange' lw 2
unset logscale y
set title ''
set ylabel ''
set xlabel 'Time'
plot [0:2] [0:5] 'simulations/sec5_n5t1b10l8' index 100000 u 1:6 \
     w lines lc 'black' lw 2,\
     theo_o(x,1.541) w lines lc 'black' lt 2,\
     'simulations/sec5_n5t1b10l8' index 100000 u 1:5 \
     w lines lc 'red' lw 2,\
     theo_o(x,1.416) w lines lc 'red',\
     'simulations/sec5_n5t1b10l8' index 100000 u 1:4 \
     w lines lc 'blue' lw 2,\
     theo_o(x,1.321) w lines lc 'blue' lt 2,\
     'simulations/sec5_n5t1b10l8' index 100000 u 1:3 \
     w lines lc 'orange' lw 2,\
     theo_o(x,0.722) w lines lc 'orange' lt 2
unset multiplot
