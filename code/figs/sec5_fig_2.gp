set terminal pngcairo size 640,480
set output "figs/sec5_fig_2.png"
set title 'Membrane potential at least θ'
set grid
set key at 0.4,450 top left spacing 2
set xlabel 'Time'
set ylabel 'Mean nb of neurons'
theo(x,v) = v
plot [0:1] [0:500] \
     'simulations/sec5_n50t10b10l5hr' index 100000 u 1:(10*$24) \
     w lines lc 'blue' lw 3 title 'x10 N=50 θ=10',\
     theo(x,125.6) w lines dashtype 0 lc 'blue' lw 2 title '', \
     'simulations/sec5_n500t100b10l5hr' index 100000 u 1:204 \
     w lines lc 'orange' lw 3 title 'x1 N=500 θ=100',\
     theo(x,119.7) w lines dashtype 0 lc 'orange' lw 2 title ''
