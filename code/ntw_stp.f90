program ntw_stp
  use iso_fortran_env, only: int32, int64, real64, &
    compiler_version, compiler_options
  use m_random ! for portable xoshiro256++ PRNG   
  implicit none
  ! Variables declaration    
  ! Define double precision real type
  integer, parameter :: dp = kind(0.0d0)
  ! Define 64 bit integer type for PRNG seed      
  integer, parameter :: i8 = selected_int_kind(18)
  type(RNG_t) :: rng ! Create a random number generator
  integer(kind=int32) :: n_neuron ! Number of neurons in the network
  integer(kind=int32) :: theta ! Membrane potential at which neurons become active
  integer(kind=i8) :: seed   ! RNG seed
  real(kind=real64) :: beta     ! Spiking rate of a neuron whose MP is >= theta
  real(kind=real64) :: lambda   ! Rate of synaptic facilitation loss
  real(kind=real64) :: period   ! Time between 2 states storage to disk
  real(kind=real64) :: duration ! Simulation duration
  integer(kind=int32) :: n_period ! ceil of duaration / period
  real(kind=real64) :: t_now        ! Current time
  real(kind=real64) :: t_next       ! Time at next state storage      
  integer(kind=int32) :: i,rep_idx  ! Loop inices
  integer(kind=int32) :: n_rep      ! Number of replicates
  ! Array state_pop contains the nb of neurones in each state      
  integer(kind=int32), allocatable, dimension(:,:) :: state_pop
  ! Array cumulated_S counts the total number of neurons (accross replica)
  ! in each of the possible state
  integer(kind=int64), allocatable, dimension(:,:,:) :: cumulated_S
  ! Array cumulated_S2 counts the total squared number of neurons (accross replica)
  ! in each of the possible state
  integer(kind=int64), allocatable, dimension(:,:,:) :: cumulated_S2
  ! Array still_alive contians the number of replicates "still alive" at each
  ! samping point
  integer(kind=int32), allocatable, dimension(:) :: still_alive
  ! Define an array storing the date and time information      
  ! integer, dimension(8) :: date_and_time_values
  ! Define a string keeping the name of the person who did the simulation      
  !character(len=200) :: who
  real(kind=real64) :: start, finish ! to get the time it took to simulate      
  ! Define strings introducing specific parts of the output
  ! Prefix of network size specification      
  character(len=*), parameter :: pre_n_neuron = "# Simulation of a networks with "
  ! Prefix of theta specification            
  character(len=*), parameter :: pre_theta = "# Parameter theta = "
  ! Prefix of number of replicates specification      
  character(len=*), parameter :: pre_n_rep = "# Number of replicates = "
  ! Prefix of simulation duration specification      
  character(len=*), parameter :: pre_duration = "# Simulation duration = "
  ! Prefix of sampling period specification      
  character(len=*), parameter :: pre_period = "# Sampling period = "
  ! Prefix identifying the beginning or the end of a replicate      
  character(len=*), parameter :: pre_rep = "# Replicate "
  ! Suffix identifying the end of a replicate            
  character(len=*), parameter :: end_rep = " done."
  ! Prefix identifying the beginning of the "statistics" part of the input      
  character(len=*), parameter :: pre_stats = "# Summary statistics: "
  ! Prefix identifying the replicate death      
  character(len=*), parameter :: pre_death = "# Dead network at time: "                 
  ! Read parameters from command line and assigned corresponding variables
  call get_par_from_cli(n_neuron, beta, lambda, theta, duration, &
      period, n_rep, seed)!, who)
  ! Writes header of the output with all the simulation parameters    
  write (*,'(a,i6,a)') pre_n_neuron, n_neuron ," neurons"
  write (*,'(a,i10,a)') "# PRNG seed set at ", seed
  write (*,'(a)') "# All neurons start in the active state."
  write (*,'(a)') "# All synapses start facilitated."
  write (*,'(a, f5.1)') "# Parameter beta = ", beta
  write (*,'(a, f5.1)') "# Parameter lambda = ", lambda
  write (*,'(a, i6)') pre_theta, theta
  if (n_rep > 1) then
    write (*,'(a, i6)') pre_n_rep, n_rep
  end if         
  write (*,'(a, f6.0)') pre_duration, duration
  write (*,'(a, f6.2)') pre_period, period      
  write (*,'(a)') " "
  ! Allocates the arrays and assign some variables 
  call seed_rng(rng,seed)
  allocate(state_pop(0:1,0:theta))
  if (period > 0) then      
    n_period = ceiling(duration/period,int32)
  else
    n_period = 1
  end if     
  ! The following allocations are only required
  ! when period > 0 and n_rep > 1
  allocate(cumulated_S(0:1,0:theta,0:n_period))
  cumulated_S = 0
  cumulated_S(1,theta,0) = n_neuron 
  allocate(cumulated_S2(0:1,0:theta,0:n_period))
  cumulated_S2 = 0
  allocate(still_alive(0:n_period))
  still_alive = 0
  still_alive(0) = n_rep     
  call cpu_time(start)      
  ! Do the real simulation job
  rep_loop: do rep_idx = 1, n_rep
    if (n_rep > 1) then
      write (*, '(a, i6)') pre_rep, rep_idx
    end if
    ! Intialize state_pop: everyone above in theta MP state
    ! with a facilitated synapse    
    state_pop = 0
    state_pop(1,theta) = n_neuron      
    t_now = 0
    ! Output initial state    
    if (period > 0) then
      write (*,'(f8.2)',advance='no') t_now
      write (*,'(*(i4))') state_pop
      t_next = period    
     else
      write (*,'(f11.3)',advance='no') t_now
      write (*,'(*(i4))') state_pop
      t_next = 0    
    end if      
          
    sampling: if (period > 0) then
      ! Store regularly
      at_period: block
        integer(kind=int32) :: period_idx ! sampling period index
        integer(kind=int32) :: multiplicity, m_idx
        period_idx = 1  
        time_loop: do
           t_now = t_now +  time_advance(state_pop,beta,lambda,rng) ! Advance in time
           if (t_now >= t_next) then
              ! If one or more sampling times are passed, output system state before updating it
              multiplicity = ceiling((t_now-t_next)/period)
              multiplicity_loop: do m_idx = 1,multiplicity
                 write (*,'(f8.2)',advance='no') t_next
                 write (*,'(*(i4))') state_pop
                 cumulated_S(:,:,period_idx) = cumulated_S(:,:,period_idx) + &
                      state_pop
                 cumulated_S2(:,:,period_idx) = cumulated_S2(:,:,period_idx) + &
                      state_pop ** 2
                 still_alive(period_idx) = still_alive(period_idx) + 1
                 period_idx = period_idx + 1
                 if (period_idx > n_period) exit 
                 t_next = t_next + period
              end do multiplicity_loop
           endif
           call one_step(state_pop, beta, lambda,rng)
           dead_yet: if (is_dead(state_pop)) then ! The system is absorbed
              write (*, '(a, f11.6)') pre_death, t_now 
              t_now = duration  
           endif dead_yet
           if (t_now >= duration) exit  ! The simulation is done
        end do time_loop
      end block at_period     
    else ! Store everything
      time_loop_all: do
        t_now = t_now + time_advance(state_pop, beta, lambda, rng) ! Advance in time
        call one_step(state_pop, beta, lambda, rng)
        write (*,'(f11.6)',advance='no') t_now
        write (*,'(*(i4))') state_pop
        dead_yet_all: if (is_dead(state_pop)) then
          write (*, '(a, f11.6)') pre_death, t_now 
          t_now = duration  
        endif dead_yet_all   
        if (t_now >= duration) exit    
      end do time_loop_all
    end if sampling          
    if (n_rep > 1) then
      write (*, '(a, i6, a)') pre_rep, rep_idx, end_rep
      write (*, '(a)') " "
      write (*, '(a)') " "    
    end if        
  end do rep_loop     
  ! Print summary stats if necessary
  if (period > 0 .and. n_rep > 1) then
     summary_stats: block
       integer(kind=int32) :: row_idx, col_idx ! loop indices
       real(kind=real64) :: denominator
       real(kind=real64), allocatable, dimension(:,:) :: mean_occupancy, &
            var_occupancy
       allocate(mean_occupancy(0:1,0:theta))
       allocate(var_occupancy(0:1,0:theta))
       write (*, '(a)') " "
       write (*, '(a)') " "
       write (*, '(a)') pre_stats
       write (*, '(f10.3, i10)', advance='no') 0.0, n_rep
       denominator = 1.0_real64
       write (*, '(*(f8.3))', advance='no') cumulated_S(:,:,0) / denominator
       write (*, '(*(f8.3))') cumulated_S2(:,:,0) / denominator
       do i = 1,n_period
          denominator = real(still_alive(i), kind=real64)
          mean_occupancy(:,:) = cumulated_S(:,:,i) / denominator
          if (still_alive(i) >= 2) then
             var_occupancy(:,:) = (cumulated_S2(:,:,i) - cumulated_S(:,:,i) ** 2 / &
                  denominator) / ( denominator - 1.0_real64)
             ! next var_occupancy becomes sem_occupancy
             var_occupancy(:,:) = sqrt(var_occupancy(:,:) / denominator)
             column_loop: do col_idx = 0,theta
                row_loopd: do row_idx = 0,1
                   call sigfig(mean_occupancy(row_idx,col_idx), &
                        var_occupancy(row_idx,col_idx))
                end do row_loopd
             end do column_loop
          end if
          write (*, '(f10.3, i10)', advance='no') i*period, still_alive(i)
          write (*, '(*(f8.3))', advance='no') mean_occupancy(:,:)
          write (*, '(*(f8.3))') var_occupancy(:,:)
       end do
     end block summary_stats
  end if         
  call cpu_time(finish)    
  
  contains
  ! A subroutine that reads the parameters and assign variables
  subroutine get_par_from_cli(n_neuron, beta, lambda, theta, duration, &
        period, n_rep, seed)!, who)
    implicit none    
    integer, intent(out) :: n_neuron, theta, n_rep
    integer(kind=i8), intent(out) :: seed    
    real(kind=real64), intent(out) :: beta, lambda, duration, period
    !character(len=200) :: who    
    character(len=6) :: n_neuron_arg
    character(len=6) :: beta_arg
    character(len=6) :: lambda_arg
    character(len=6) :: theta_arg
    character(len=6) :: duration_arg
    character(len=6) :: period_arg
    character(len=25) :: n_rep_arg    
    character(len=25) :: seed_arg
  
    if (command_argument_count() == 0) then
      write (*,'(a)') "This program expects 9 parameters:"
      write (*,'(a)') "ntw_stp n_neuron beta lambda theta duration " // &
            "period n_rep seed who"
      write (*,'(a)') "  n_neuron (positive integer): number of neurons"
      write (*,'(a)') "  beta (positive real): see below"
      write (*,'(a)') "  lambda (positive real): see below"
      write (*,'(a)') "  theta (positive integer): see below"
      write (*,'(a)') "  duration (positive real): the duration to simulate"
      write (*,'(a)') "  period (positive real): the sampling period"
      write (*,'(a)') "  n_rep (positive integer): the number of replicates"      
      write (*,'(a)') "  seed (integer): the rng seed"
      !write (*,'(a)') "  who (string): the name of the person who ran the sim."      
      write (*,'(a)') " "  
      write (*,'(a)') "Simulates an homogenous network made of n_neuron neurons."
      write (*,'(a)') "Two variables are associated with each neuron:"
      write (*,'(a)') "  a 'membrane potential' U,"
      write (*,'(a)') "  a 'synaptic facilitation' F."
      write (*,'(a)') "The neurons spike independently of each other with a"
      write (*,'(a)') "rate beta once their membrane potential equals or"
      write (*,'(a)') "exceeds a threshold theta."
      write (*,'(a)') "The neuron that spiked increases the membrane potential"
      write (*,'(a)') "of all the other neurons by 1 if it's synapse was"
      write (*,'(a)') "facilitated, 0 otherwise."
      write (*,'(a)') "After the spike, the facilitation of the neuron"
      write (*,'(a)') "that spiked is set to 1 (facilitated) if it was 0"
      write (*,'(a)') "(unfacilitated) and stays at 1 otherwise."
      write (*,'(a)') "The membrane potential of the neuron that just spiked"
      write (*,'(a)') "is set to 0."
      write (*,'(a)') "Independently of spike events the facilitation of each"
      write (*,'(a)') "neuron drops to 0 exponentially with rate lambda."
      write (*,'(a)') " "
      write (*,'(a)') "If period is set to 0, the network state is written"
      write (*,'(a)') "to the sdout after each event (spike or facilitation loss);"
      write (*,'(a)') "it is written with an interval given by period otherwise."
      write (*,'(a)') "If n_rep is larger than one, n_rep replicates are"
      write (*,'(a)') "simulated one after the other. The mean occupancy number"
      write (*,'(a)') "as well as its standard error are written after each"
      write (*,'(a)') "sampling period on two successive lines at the end"
      write (*,'(a)') "of the output."            
      stop        
    else
      call get_command_argument(1, n_neuron_arg)
      read (n_neuron_arg, *) n_neuron
      if (n_neuron <= 1) stop "The number of neurons (n_neuron) should be larger than 1."      
      call get_command_argument(2, beta_arg)
      read (beta_arg, *) beta
      if (beta <= 0) stop "The spiking rate (beta) should be > 0."      
      call get_command_argument(3, lambda_arg)
      read (lambda_arg, *) lambda
      if (lambda <= 0) stop "The facilitation loss rate (lambda) should be > 0."     
      call get_command_argument(4, theta_arg)
      read (theta_arg, *) theta
      if (theta < 1) stop "The activation threshold (theta) should at least 1."
      if (theta >= n_neuron) stop "The activation threshold (theta) should be " // &
            "smaller than n_neuron."      
      call get_command_argument(5, duration_arg)
      read (duration_arg, *) duration
      if (duration <= 0) stop "Duration should be > 0."      
      call get_command_argument(6, period_arg)
      read (period_arg, *) period
      if (period < 0.01) stop "The sampling period should be >= 0.01."
      if (period >= duration) stop "A sampling period larger than the reocording!"
      call get_command_argument(7, n_rep_arg)
      read (n_rep_arg, *) n_rep
      if (n_rep < 1) stop "n_rep should be >= 1."
      call get_command_argument(8, seed_arg)
      read (seed_arg, *) seed
      !call get_command_argument(9, who)           
    end if      
      
  end subroutine get_par_from_cli          
  ! Seeds the (pseudo-)random number generator
  subroutine seed_rng(rng,seed)
    implicit none
    type(RNG_t), intent(in out) :: rng    
    integer(kind=i8), intent(in) :: seed
    integer(kind=int32), parameter :: n_discard = 100
    integer :: i
    real(kind=dp) :: ran
    call rng%set_seed([seed, seed+49927_i8])    
    do i=1,n_discard
      ran = rng%unif_01()
    end do
  end subroutine seed_rng     
  ! Generates the time interval to the next event (spike or facilitation loss)
  real(kind=real64) function time_advance(state_array,beta,lambda,rng) result(dt)
    implicit none
    type(RNG_t), intent(in out) :: rng ! PRNG object    
    integer(kind=int32), allocatable, dimension(:,:), intent (in) :: state_array
    real(kind=real64), intent (in) :: beta   ! Spiking rate of a neuron whose MP
                                             ! is >= theta
    real(kind=real64), intent (in) :: lambda ! Rate of synaptic facilitation loss  
    integer(kind=int32) :: theta             ! Spiking threshold
    real(kind=real64) :: ntw_rate            ! Rate at which the whole network
                                             ! generates events
    !real(kind=real64) :: unif_rand           ! Result of uniform PRNG draw
        
    theta = ubound(state_array,DIM=2)
    ntw_rate = beta * sum(state_array(:,theta)) + lambda * sum(state_array(1,:))
    !call random_number(unif_rand)
    !dt = - dlog(unif_rand) / ntw_rate
    dt = rng%exponential(ntw_rate)    
  end function time_advance
  ! Assign the event type and makes the system state evolve accordingly    
  subroutine one_step(state_array, beta, lambda, rng)
    implicit none
    type(RNG_t), intent(in out) :: rng ! PRNG object        
    integer(kind=int32), allocatable, dimension(:,:), intent (in out) :: state_array
    real(kind=real64), intent (in) :: beta   ! Spiking rate of a neuron whose
                                             ! MP is >= theta
    real(kind=real64), intent (in) :: lambda ! Rate of synaptic facilitation loss  
    real(kind=real64) :: ntw_rate   ! Rate at which the whole network generates events
    real(kind=real64) :: spike_rate ! The global spiking rate of the network
    real(kind=real64) :: unif_rand  ! contains a PRNG draw   
    integer(kind=int32) :: theta, theta_dot, facil_dot
    real(kind=real64) :: facil_dot_real, ratio
        
    theta = ubound(state_array,DIM=2)     ! Membrane potential threshold for spiking
    theta_dot = sum(state_array(:,theta)) ! Number of spiking neurons    
    spike_rate = beta * real(theta_dot)   
    facil_dot = sum(state_array(1,:))     ! Number of facilitated synapses
    facil_dot_real = real(facil_dot, kind=real64)  
    ntw_rate = lambda * facil_dot_real + spike_rate ! Global event rate   
    !call random_number(unif_rand)
    unif_rand = rng%unif_01()    
    what_event: if (spike_rate > unif_rand * ntw_rate) then
      ! The event is a spike
      !call random_number(unif_rand)
      unif_rand = rng%unif_01()      
      is_suceptible: if ( state_array(1,theta) > unif_rand * theta_dot) then
        ! The synapse is facilitated
        ! make a circular shift of state_array rows by 1 step to the right  
        state_array = cshift(state_array,SHIFT=-1,DIM=2)  
        state_array(1,theta) = state_array(1,theta) - 1 + state_array(1,0)
        state_array(0,theta) = state_array(0,theta) + state_array(0,0)  
        state_array(1,0) = 1
        state_array(0,0) = 0  
      else ! The synapse is not facilitated
        state_array(0,theta) = state_array(0,theta) - 1
        state_array(1,0) = state_array(1,0) + 1  
      end if is_suceptible  
    else ! The event is a synaptic facilitation loss
      !call random_number(unif_rand)
      unif_rand = rng%unif_01()      
      block
        integer(kind=int32) :: mem_pot    
        mem_pot = theta
        who_looses_facilitation: do
          ! Find out the membrane potential of the neuron loosing facilitation
          if (state_array(1,mem_pot) > 0) then
            ratio = state_array(1,mem_pot) / facil_dot_real
            if (ratio >= unif_rand) exit
            unif_rand = unif_rand - ratio
          end if  
          mem_pot = mem_pot - 1
        end do who_looses_facilitation
        state_array(1,mem_pot) = state_array(1,mem_pot) - 1
        state_array(0,mem_pot) = state_array(0,mem_pot) + 1    
        facil_dot = facil_dot - 1
      end block          
  end if what_event
  
  end subroutine one_step    
  ! Check if a state is 'dead'
  function is_dead(state_array) result(res)
    implicit None
    integer(kind=int32), allocatable, dimension(:,:), intent (in) :: state_array
    integer(kind=int32) :: theta, i
    logical :: res, in_r_prime
    
    theta = ubound(state_array,DIM=2)     ! Membrane potential threshold for spiking
    in_r_prime = any(sum(state_array(:,0:theta-1),dim=1) == 0) ! Check if in R'
    check_R_prime: If (in_r_prime) Then ! The system is not absorbed
       res = .false.
    Else ! The system could be absorbed
       res = state_array(1,theta) == 0 ! If .true. the system is absorbed
       x_1_theta_populated: If (.not. res) Then ! The sytem could still be absorded
          i = theta-1
          reset_not_reached: if (i > 0) then
             lower_potential_loop: do
                res = sum(state_array(1,i:theta)) <= theta - i
                i = i-1
                if (res .or. i==0) exit
             end do lower_potential_loop
          end If reset_not_reached
          last_possibility: If (.not. res .and. i==0) Then
             res = (state_array(0,theta) + sum(state_array(1,0:theta))) <= theta
          End If last_possibility
       End If x_1_theta_populated
    End If check_R_prime
  end function is_dead
  ! Rounds two numbers, like a mean and its SE rounded to th, their least
  ! significant digit
  subroutine sigfig(val, sigma)
    implicit none    
    real(kind=real64), intent(in out) :: val
    real(kind=real64), intent(in out) :: sigma
    integer(kind=int32) :: power_10
    if (sigma >= 1) then
      power_10 = ceiling(dlog10(sigma))
    else
      power_10 = floor(dlog10(sigma))  
    end if    
    sigma = 10.0**power_10 * ceiling(sigma/10.0**power_10)
    val = 10.0**power_10 * IDNINT(val/10.0**power_10)    
  end subroutine sigfig          
end program ntw_stp
