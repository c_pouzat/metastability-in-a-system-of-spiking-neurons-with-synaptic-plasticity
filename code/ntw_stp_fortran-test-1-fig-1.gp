set terminal pngcairo size 640,640
set output "figs/ntw_stp_fortran_test_1_fig_1.png"
set title 'Membrane potential at least θ'
set grid
set key at 55,17.7 top left spacing 1.5
set xlabel 'Time'
set ylabel 'Mean nb of neurons'
set bars small
plot [] [15:25] 'simulations/test_fortran' index 1000 u 1:14:(2*$26) \
     w yerrorlines pt 0 lc 'black' lw 2 title '(u,f)=(θ,1) Fortran',\
     'simulations/test_python' index 1000 u 1:14 \
     w lines lc 'orange' lw 2 title '(u,f)=(θ,1) Python',\
     'simulations/test_fortran' index 1000 u 1:13:(2*$25) \
     w yerrorlines pt 0 lc 'red' lw 2 title '(u,f)=(θ,0) Fortran',\
     'simulations/test_python' index 1000 u 1:13 \
     w lines lc 'blue' lw 2 title '(u,f)=(θ,0) Python'
