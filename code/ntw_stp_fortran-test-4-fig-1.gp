set terminal pngcairo size 640,540
set output "figs/ntw_stp_fortran_test_4_fig_1.png"
set title 'Membrane potential at least θ'
set grid
set key at 90,210 top left spacing 2
set xlabel 'Time'
set ylabel 'Mean nb of neurons'
set bars small
theo(x,val) = val
plot [] [170:230] 'simulations/test_fortran_N500' index 10000 u 1:104:(2*$206) \
     w yerrorbars pt 2 lc 'black' lw 2 title '(u,f)=(θ,1) 10000 Rep.',\
     theo(x,223.26) w lines lc 'orange' title '(u,f)=(θ,1) Analytical',\
     'simulations/test_fortran_N500' index 10000 u 1:103:(2*$205) \
     w yerrorbars pt 2 lc 'red' lw 2 title '(u,f)=(θ,0) 10000 Rep.',\
     theo(x,185.25) w lines lc 'blue' title '(u,f)=(θ,0) Analytical'
