set terminal pngcairo size 640,640
set output "figs/ntw_stp_fortran_test_2_fig_2.png"
set title 'Membrane potential at least θ'
set grid
set key at 100,21 top left spacing 2
set xlabel 'Time'
set ylabel 'Mean nb of neurons'
set bars small
plot [] [17:23] 'simulations/test_fortran_2' index 10000 u 1:14:(2*$26) \
     w yerrorbars pt 2 lc 'black' lw 2 title '(u,f)=(θ,1) 10000 Rep.',\
     'simulations/test_fortran' index 1000 u 1:14:26 \
     w lines lc 'orange' lw 2 title '(u,f)=(θ,1) 1000 Rep.',\
     'simulations/test_fortran_2' index 10000 u 1:13:(2*$25) \
     w yerrorbars pt 2 lc 'red' lw 2 title '(u,f)=(θ,0) 10000 Rep.',\
     'simulations/test_fortran' index 1000 u 1:13 \
     w lines lc 'blue' lw 2 title '(u,f)=(θ,0) 1000 Rep.'
